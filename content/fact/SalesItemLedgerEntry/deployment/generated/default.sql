SELECT
  -- system information
  ILE.stage_id AS StageID
  , ILE.Company_Id AS CompanyID
  , ILE.data_connection_id AS DataConnectionID
  -- business key
  , ILE.[Entry No_] AS ItemLedgerEntryNo
  -- dimensions
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , ILE.[Item No_] AS ItemCode
  , SILE.DocumentPostingDate AS DocumentPostingDate
  , ILE.[Posting Date] AS PostingDate
  , SILE.SellToCustomerNo AS SellToCustomerCode
  , COALESCE(SILE.BillToCustomerNo, SILE.SellToCustomerNo) AS BillToCustomerCode
  , ILE.[Document Type] AS DocumentTypeCode
  , ILE.[Document No_] AS DocumentCode
  , ILE.[Location Code] AS LocationCode
  , ILE.[Variant Code] AS ItemVariantCode
  , ILE.[Return Reason Code] AS ReturnReasonCode
  , ILE.[Transport Method] AS TransportMethodCode
  /* ISF */
  , ILE.[IsfBrand Code] AS BrandCode
  , ILE.[IsfCollection Code] AS CollectionCode
  , ILE.[IsfColor Code] AS ColorCode
  , ILE.[IsfSeason Code] AS SeasonCode
  , ILE.[IsfStyle No_] AS StyleCode
  , ILE.[IsfOrder Type] AS OrderTypeCode
  , ILE.[IsfCup Code] AS CupCode
  , ILE.[IsfWidth Code] AS WidthCode
  , ILE.[IsfLength] AS LengthCode
  , ILE.[IsfOrder Origin] AS OrderOriginCode
  , ILE.[IsfQuality Code] AS QualityCode
  , ILE.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures                                                                         
  , ILE.[Quantity] AS ShippedQuantity
  , ILE.[Remaining Quantity] AS RemainingQuantity
  /*ISF*/
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * ILE.Quantity)
    ELSE NULL
    END AS PrepackQuantity
/*ISF*/
FROM stage_nav.[Item Ledger Entry] AS ILE
INNER JOIN help.[SalesItemLedgerEntry] AS SILE
  ON ILE.[Entry No_] = SILE.EntryNo
    AND ILE.Company_Id = SILE.Company_ID
    AND ILE.Data_connection_id = SILE.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON ILE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = ILE.company_id
    AND ER.DataConnectionID = ILE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON ILE.[Dimension Set ID] = FD.dimensionsetno
    AND ILE.company_id = FD.companyid
    AND ILE.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON ILE.[Item No_] = IV.[Item No_]
    AND ILE.[Variant Code] = IV.Code
    AND ILE.Company_Id = IV.Company_Id
    AND ILE.Data_Connection_Id = IV.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = ILE.[Item No_]
    AND PRE.[Prepack Code] = ILE.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.Company_Id = ILE.Company_Id
    AND PRE.Data_Connection_Id = ILE.Data_Connection_Id
    AND PRE.[Type] = 2 --prepack content
    /*ISF*/
WHERE ILE.[Entry Type] = 1 --Sales
