SELECT
  -- system information
  WE.stage_id AS StageID
  , WE.company_id AS CompanyID
  , WE.data_connection_id AS DataConnectionID
  --dimensions
  , [Location Code] AS LocationCode
  , [Zone Code] AS ZoneCode
  , [Bin Code] AS BinCode
  , [Bin Type Code] AS BinTypeCode
  , [Item No_] AS ItemCode
  , [Variant Code] AS ItemVariantCode
  , [Registering Date] AS RegisteringDate
  , [Warranty Date] AS WarrantyDate
  , [Expiration Date] AS ExpirationDate
  , [Source Type] AS SourceType
  , [Source Code] AS SourceCode
  , [Reason Code] AS ReasonCode
  , [Entry No_] AS EntryNo
  , [Line No_] AS WarehouseLineNo
  , [Whse_ Document No_] AS WarehouseDocumentCode
  , [Whse_ Document Type] AS WarehouseDocumentTypeCode
  , [Entry Type] AS EntryType
  , [Lot No_] AS LotCode
  , [User ID] AS WarehouseEmployeeCode
  -- measures
  , Quantity AS WarehouseEntryQuantity
  , [Qty_ (Base)] AS WarehouseEntryQuantityBase
  , Cubage AS WarehouseEntryCubage
  , [Weight] AS WarehouseEntryWeight
FROM stage_nav.[Warehouse Entry] AS WE
