SELECT
  -- system information
  VE.stage_id AS StageID
  , VE.Company_Id AS CompanyID
  , VE.data_connection_id AS DataConnectionID
  -- dimensions
  , VE.[Item Ledger Entry No_] AS ItemLedgerEntryCode
  , VE.[Entry No_] AS ValueEntryCode
  , VE.[Item Ledger Entry Type] AS ItemLedgerEntryTypeCode
  , VE.[Item No_] AS ItemCode
  , VE.[Posting Date] AS PostingDate
  , VE.[Source Code] AS SourceCode
  , VE.[Document No_] AS DocumentNo
  , VE.[Document Type] AS DocumentTypeCode
  , VE.[Location Code] AS LocationCode
  , VE.[Inventory Posting Group] AS InventoryPostingGroupCode
  , VE.[Source Posting Group] AS SourcePostingGroupCode
  , VE.[Salespers__Purch_ Code] AS SalespersonPurchaserCode
  , VE.[Source Type] AS SourceTypeCode
  , VE.[Reason Code] AS ReasonCode
  , VE.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , VE.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , VE.[Item Charge No_] AS ItemChargeCode
  , VE.Type AS Type
  , VE.No_ AS No
  , VE.[Variant Code] AS ItemVariantCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , VE.[IsfBrand Code] AS BrandCode
  , VE.[IsfCollection Code] AS CollectionCode
  , VE.[IsfColor Code] AS ColorCode
  , VE.[IsfSeason Code] AS SeasonCode
  , VE.[IsfStyle No_] AS StyleCode
  , VE.[IsfOrder Type] AS OrderTypeCode
  , VE.[IsfCup Code] AS CupCode
  , VE.[IsfWidth Code] AS WidthCode
  , VE.[IsfLength] AS LengthCode
  , VE.[IsfOrder Origin] AS OrderOriginCode
  , VE.[IsfQuality Code] AS QualityCode
  , VE.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures
  , VE.[Purchase Amount (actual)] AS ReceiptAmountLCY
  , VE.[Sales Amount (actual)] AS IssueAmountLCY
  , VE.[Cost Posted to G_L] AS CostPostedtoGLLCY
  , VE.[Cost Amount (Expected)] + [Cost Amount (Actual)] AS CostAmountExpectedLCY
  , VE.[Cost Posted to G_L] + [Expected Cost Posted to G_L] AS CostPostedToGLExpectedLCY
  , CASE 
    WHEN VE.[Item Ledger Entry Type] IN (
        0
        , 2
        , 6
        )
      THEN VE.[Invoiced Quantity]
    ELSE NULL
    END AS PositiveInvoicedQuantity
  , CASE 
    WHEN VE.[Item Ledger Entry Type] IN (
        1
        , 3
        , 5
        )
      THEN VE.[Invoiced Quantity]
    ELSE NULL
    END AS NegativeInvoicedQuantity
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Cost Amount (Actual)]
    ELSE NULL
    END AS CostAmountLCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      AND VE.[Cost Amount (Actual)] >= 0
      THEN VE.[Cost Amount (Actual)]
    ELSE NULL
    END AS PositiveCostAmountLCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      AND VE.[Cost Amount (Actual)] < 0
      THEN VE.[Cost Amount (Actual)]
    ELSE NULL
    END AS NegativeCostAmountLCY
  -- ACY Measures
  , VE.[Purchase Amount (actual)] * ER.CrossRate AS ReceiptAmountRCY
  , VE.[Sales Amount (actual)] * ER.CrossRate AS IssueAmountRCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Cost Amount (Actual)] * ER.CrossRate
    ELSE NULL
    END AS CostAmountRCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      AND VE.[Cost Amount (Actual)] >= 0
      THEN VE.[Cost Amount (Actual)] * ER.CrossRate
    ELSE NULL
    END AS PositiveCostAmountRCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      AND VE.[Cost Amount (Actual)] < 0
      THEN - VE.[Cost Amount (Actual)] * ER.CrossRate
    ELSE NULL
    END AS NegativeCostAmountRCY
  , VE.[Cost Posted to G_L] * ER.CrossRate AS CostPostedToGLRCY
  , VE.[Cost Amount (Expected)] * ER.CrossRate AS CostAmountExpectedRCY
  , VE.[Expected Cost Posted To G_L] * ER.CrossRate AS CostPostedToGLExpectedRCY
FROM Stage_Nav.[Value Entry] AS VE
LEFT JOIN help.ExchangeRates AS ER
  ON VE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = VE.company_id
    AND ER.DataConnectionID = VE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON VE.[Dimension Set ID] = FD.DimensionSetNo
    AND VE.company_id = FD.CompanyID
    AND VE.data_connection_Id = FD.DataConnectionID
WHERE VE.[Item Ledger Entry Type] <> 7
