WITH LastApplicationDate_CTE
AS (
  SELECT company_id AS CompanyID
    , data_connection_id AS DataConnectionID
    , [Vendor Ledger Entry No_] AS VendorLedgerEntryNo
    , MAX([Posting Date]) AS LastApplicationDate
  FROM stage_nav.[Detailed Vendor Ledg_ Entry]
  WHERE [Entry Type] = 2 --Application
  GROUP BY company_id
    , data_connection_id
    , [Vendor Ledger Entry No_]
  )
SELECT
  -- system information
  DVLE.stage_id AS StageID
  , DVLE.company_id AS CompanyID
  , DVLE.data_connection_id AS DataConnectionID
  -- dimensions                                                                                                                    
  , DVLE.[Vendor No_] AS PayToVendorCode
  , VLE.[Buy-from Vendor No_] AS BuyFromVendorCode
  , VLE.[Vendor Posting Group] AS VendorPostingGroupCode
  , DVLE.[Vendor Ledger Entry No_] AS VendorInvoiceCode
  , VLE.[Purchaser Code] AS SalespersonPurchaserCode
  , DVLE.[Posting Date] AS PostingDate
  , COALESCE(NULLIF(VLE.[Due Date], '1753-01-01 00:00:00'), VLE.[Posting Date]) AS DueDate
  , CASE 
    WHEN VLE.[Open] = 1
      THEN NULL
    ELSE COALESCE(NULLIF(VLE.[Closed at Date], '1753-01-01 00:00:00'), NULLIF(LAD.[LastApplicationDate], '1753-01-01 00:00:00'))
    END AS PaidDate
  , DVLE.[Currency Code] AS CurrencyCode
  , CASE 
    WHEN DVLE.[Entry Type] = 1
      AND VLE.[Document Type] IN (
        2
        , 3
        )
      THEN 1
    ELSE 0
    END AS IsInvoiceCode
  , CASE 
    WHEN DVLE.[Credit Amount (LCY)] <> 0
      THEN 1
    ELSE 0
    END AS IsCreditCode
  , COALESCE(CASE 
      WHEN DVLE.[Entry Type] = 1
        AND VLE.[Document Type] IN (
          2
          , 3
          )
        THEN 1
      ELSE 0
      END, - 1) AS IsInvoice
  , COALESCE(CASE 
      WHEN DVLE.[Credit Amount (LCY)] <> 0
        THEN 1
      ELSE 0
      END, - 1) AS IsCredit
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , DVLE.[Amount (LCY)] * - 1 AS Amount_LCY
  , DVLE.[Debit Amount (LCY)] AS AmountDebit_LCY
  , DVLE.[Credit Amount (LCY)] AS AmountCredit_LCY
  , CASE 
    WHEN DVLE.[Entry Type] = 1
      THEN VLE.[Inv_ Discount (LCY)]
    ELSE 0
    END AS AmountDiscount_LCY
  -- measures_RCY
  , DVLE.[Amount (LCY)] * - 1 * ER.CrossRate AS Amount_RCY
  , DVLE.[Debit Amount (LCY)] * ER.CrossRate AS AmountDebit_RCY
  , DVLE.[Credit Amount (LCY)] * ER.CrossRate AS AmountCredit_RCY
  , CASE 
    WHEN DVLE.[Entry Type] = 1
      THEN VLE.[Inv_ Discount (LCY)]
    ELSE 0
    END * ER.CrossRate AS AmountDiscount_RCY
  -- measures_PCY
  , DVLE.Amount * - 1 AS Amount_PCY
FROM stage_nav.[Detailed Vendor Ledg_ Entry] AS DVLE
INNER JOIN stage_nav.[Vendor Ledger Entry] AS VLE
  ON VLE.[Entry No_] = DVLE.[Vendor Ledger Entry No_]
    AND VLE.company_id = DVLE.company_id
    AND VLE.data_connection_id = DVLE.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON DVLE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = DVLE.company_id
    AND ER.DataConnectionID = DVLE.data_connection_id
LEFT JOIN LastApplicationDate_CTE AS LAD
  ON VLE.[Entry No_] = LAD.VendorLedgerEntryNo
    AND VLE.company_id = LAD.CompanyID
    AND VLE.data_connection_id = LAD.DataConnectionID
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON VLE.[Dimension Set ID] = FD.DimensionSetNo
    AND VLE.company_id = FD.CompanyID
    AND VLE.data_connection_id = FD.DataConnectionID
