SELECT
  -- system information
  PWRL.stage_id AS StageID
  , PWRL.company_id AS CompanyID
  , PWRL.data_connection_id AS DataConnectionID
, 0 AS WarehouseType
  --dimensions
  , PWRL.[No_] AS WarehouseDocumentCode
  , PWRL.[Location Code] AS LocationCode
  , PWRL.[Status] AS [Status]
  , PWRL.[Source Document] AS SourceDocument
  , PWRL.[Source No_] AS SourceCode
  , PWRL.[Bin Code] AS BinCode
  , PWRL.[Zone Code] AS ZoneCode
  , PWRL.[Item No_] AS ItemCode
  , PWRL.[Lot No_] AS LotCode
  , PWRL.[Variant Code] AS ItemVariantCode
  , PWRH.[Assigned User ID] AS WarehouseEmployeeCode
  , PWRH.[Posting Date] AS PostingDate
  , PWRL.[Warranty Date] AS WarrantyDate
  , PWRL.[Expiration Date] AS ExpirationDate
  , 1 AS WarehouseDocumentTypeCode -- receipt
  , - 1 AS ActionTypeCode
  , - 1 AS ActivityTypeCode
  -- measures
  , PWRL.[Qty_ (Base)] AS WarehouseQuantityBase
  , PWRL.[Qty_ Put Away (Base)] AS WarehouseQuantityPutAwayBase
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseCubage
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseWeight
FROM stage_nav.[Posted Whse_ Receipt Line] PWRL
INNER JOIN stage_nav.[Posted Whse_ Receipt Header] PWRH
  ON PWRL.[No_] = PWRH.[No_]
    AND PWRL.company_id = PWRH.company_id
    AND PWRL.data_connection_id = PWRH.data_connection_id
