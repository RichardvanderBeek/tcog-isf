SELECT
  -- system information
  RWAL.stage_id AS StageID
  , RWAL.company_id AS CompanyID
  , RWAL.data_connection_id AS DataConnectionID
  , 2 AS WarehouseType
  --dimensions
  , RWAL.[No_] AS WarehouseDocumentCode
  , RWAL.[Location Code] AS LocationCode
  , - 1 AS [Status]
  , RWAL.[Source Document] AS SourceDocument
  , RWAL.[Source No_] AS SourceCode
  , RWAL.[Bin Code] AS BinCode
  , RWAL.[Zone Code] AS ZoneCode
  , RWAL.[Item No_] AS ItemCode
  , CAST(NULL AS NVARCHAR(20)) AS LotCode
  , RWAL.[Variant Code] AS ItemVariantCode
  , RWAH.[Assigned User ID] AS WarehouseEmployeeCode
  , RWAH.[Registering Date] AS PostingDate
  , RWAL.[Warranty Date] AS WarrantyDate
  , RWAL.[Expiration Date] AS ExpirationDate
  , 2 + RWAH.[Type] AS WarehouseDocumentTypeCode -- Whse. Activity
  , [Action Type] AS ActionTypeCode
  , [Activity Type] AS ActivityTypeCode
  -- measures
  , RWAL.[Qty_ (Base)] AS WarehouseQuantityBase
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseQuantityPutAwayBase
  , RWAL.Cubage AS WarehouseCubage
  , RWAL.[Weight] AS WarehouseWeight
FROM stage_nav.[Registered Whse_ Activity Line] RWAL
INNER JOIN stage_nav.[Registered Whse_ Activity Hdr_] RWAH
  ON RWAL.[No_] = RWAH.[No_]
    AND RWAL.company_id = RWAH.company_id
    AND RWAL.data_connection_id = RWAH.data_connection_id
