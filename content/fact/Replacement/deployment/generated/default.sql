SELECT
  -- system information
  IRE.stage_id AS StageID
  , COALESCE(PC.stage_id, 1) AS PrepackStageID
  , IRE.Company_Id AS CompanyID
  , IRE.data_connection_id AS DataConnectionID
  , IRE.[Entry No_] AS EntryCode
  , IRE.[Document No_] AS ReplacementDocumentCode
  , IRE.[Document Line No_] AS ReplacementDocumentLineCode
  , IRE.[Document Type] AS ReplacementDocumentTypeCode
  -- dimensions
  , IRE.[Brand] AS BrandCode
  , IRE.[Collection] AS CollectionCode
  , IRE.[Season Code] AS SeasonCode
  , IRE.[Color Code] AS ColorCode
  , IRE.[Style No_] AS StyleCode
  , IRE.[Prepack Code] AS PrepackCode
  , IRE.[Length] AS LengthCode
  --, IRE.[Order Origin] AS OrderOriginCode
  , IRE.[Width Code] AS WidthCode
  , IRE.[Variant Code] AS ItemVariantCode
  , IRE.[Item No_] AS ItemCode
  , IRE.[Buy-from Vendor No_] AS BuyFromVendorCode
  , IRE.[Posting Date] AS PostingDate
  , IRE.[Reason Code] AS ReasonCode
  , IRE.[Replacement No_] AS ReplacementNo
  , IRE.[Sell-to Customer No_] AS SelltoCustomerCode
  , CAST(0 AS DECIMAL(38, 20)) AS Quantity
  , CAST(0 AS DECIMAL(38, 20)) AS QuantityBase
  , PC.Quantity AS PrepackQuantity
FROM stage_nav.[IsfReplacement Entry] AS IRE
LEFT JOIN stage_nav.[IsfPrepack Content] AS PC
  ON IRE.[Item No_] = PC.[No_]
    AND IRE.Company_ID = PC.Company_ID
    AND IRE.Data_Connection_ID = PC.Data_Connection_ID
    AND PC.[Type] = 2 --prepack content    
