SELECT
  -- system information
  SCML.stage_id AS StageID
  , SCML.company_id AS CompanyID
  , SCML.data_connection_id AS DataConnectionID
  , 3 AS SalesValueEntryType
  -- business key
  , SCML.[Document No_] AS DocumentNo
  , SCML.Type AS SalesLineTypeCode
  , 0 AS ItemLedgerEntryNo
  , 0 AS ValueEntryNo
  -- Other fields
  , 0 AS SourceType
  , CAST(NULL AS NVARCHAR(20)) AS SourcePostingGroup
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- dimensions                                                          
  , CAST(NULL AS NVARCHAR(20)) AS ItemCode --comes from Value Entry table
  , CASE 
    WHEN SCML.Type = 1
      THEN SCML.No_
    ELSE ''
    END AS GeneralLedgerAccountCode
  , CASE 
    WHEN SCML.Type = 3
      THEN SCML.No_
    ELSE ''
    END AS ResourceCode
  , CASE 
    WHEN SCML.Type = 4
      THEN SCML.No_
    ELSE ''
    END AS FixedAssetCode
  , CASE 
    WHEN SCML.Type = 5
      THEN SCML.No_
    ELSE ''
    END AS ItemChargeCode
  , SCML.[Posting Date] AS DocumentPostingDate
  , SCMH.[Posting Date] AS PostingDate
  , SCMH.[Sell-to Customer No_] AS SellToCustomerCode
  , SCMH.[Bill-to Customer No_] AS BillToCustomerCode
  , 4 AS DocumentTypeCode -- Credit Memo
  , SCML.[Location Code] AS LocationCode
  , SCML.[Variant Code] AS ItemVariantCode
  , SCMH.[Source Code] AS SourceCode
  , SCML.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , SCML.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , CAST(NULL AS NVARCHAR(20)) AS ReturnReasonCode
  , SCMH.[Reason Code] AS ReasonCode
  , SCMH.[Salesperson Code] AS SalesPersonPurchaserCode
  , SCMH.[Currency Code] AS CurrencyCode
  , CAST(NULL AS NVARCHAR(20)) AS InventoryPostingGroupCode
  /* ISF */
  , SCML.[IsfBrand Code] AS BrandCode
  , SCML.[IsfCollection Code] AS CollectionCode
  , SCML.[IsfColor Code] AS ColorCode
  , SCML.[IsfSeason Code] AS SeasonCode
  , SCML.[IsfStyle No_] AS StyleCode
  , SCML.[IsfOrder Type] AS OrderTypeCode
  , SCML.[IsfCup Code] AS CupCode
  , SCML.[IsfWidth Code] AS WidthCode
  , SCML.[IsfLength Code] AS LengthCode
  , SCML.[IsfOrder Origin] AS OrderOriginCode
  , SCML.[IsfQuality Code] AS QualityCode
  , SCML.[IsfPrepack Code] AS PrepackCode
  , '' AS ShippingWindowCode
  /* ISF */
  -- measures
  , CASE 
    WHEN SCML.Type <> 1
      THEN SCML.[Quantity (Base)] * - 1
    ELSE 0
    END AS InvoicedQuantity -- exclude GL lines
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.Amount / SCMH.[Currency Factor]) * - 1
    ELSE SCML.Amount * - 1
    END AS SalesAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_LCY
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.[Line Discount Amount] / SCMH.[Currency Factor]) * - 1
    ELSE SCML.[Line Discount Amount] * - 1
    END AS DiscountAmount_LCY
  , SCML.[Unit Cost (LCY)] * SCML.[Quantity (Base)] * - 1 AS CostAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.Amount / SCMH.[Currency Factor]) * ER.CrossRate * - 1
    ELSE SCML.Amount * ER.CrossRate * - 1
    END AS SalesAmount_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_RCY
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.[Line Discount Amount] / SCMH.[Currency Factor]) * ER.CrossRate * - 1
    ELSE SCML.[Line Discount Amount] * ER.CrossRate * - 1
    END AS DiscountAmount_RCY
  , CASE 
    WHEN SCML.Type <> 1
      THEN SCML.[Unit Cost] * SCML.[Quantity (Base)] * ER.CrossRate * - 1
    ELSE 0
    END AS CostAmount_RCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.Amount / SCMH.[Currency Factor]) * - 1
    ELSE SCML.Amount * - 1
    END AS SalesAmount_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_PCY
  , CASE 
    WHEN SCMH.[Currency Factor] <> 0
      THEN (SCML.[Line Discount Amount] / SCMH.[Currency Factor]) * - 1
    ELSE SCML.[Line Discount Amount] * - 1
    END AS DiscountAmount_PCY
  , CASE 
    WHEN SCML.Type <> 1
      THEN SCML.[Unit Cost] * SCML.[Quantity (Base)] * - 1
    ELSE 0
    END AS CostAmount_PCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_PCY
  /*ISF*/
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * SCML.[Quantity (Base)])
    ELSE NULL
    END AS PrepackQuantity
/*ISF*/
FROM stage_nav.[Sales Cr_Memo Line] AS SCML
LEFT JOIN stage_nav.[Sales Cr_Memo Header] AS SCMH
  ON SCML.[Document No_] = SCMH.No_
    AND SCML.company_id = SCMH.company_id
    AND SCML.data_connection_id = SCMH.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON SCMH.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = SCMH.company_id
    AND ER.DataConnectionID = SCMH.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON SCML.[Dimension Set ID] = FD.dimensionsetno
    AND SCML.company_id = FD.CompanyID
    AND SCML.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON SCML.No_ = IV.[Item No_]
    AND SCML.[Variant Code] = IV.Code
    AND SCML.Company_Id = IV.Company_Id
    AND SCML.Data_Connection_Id = IV.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = SCML.No_
    AND PRE.[Prepack Code] = SCML.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.Company_Id = SCML.Company_Id
    AND PRE.Data_Connection_Id = SCML.Data_Connection_Id
    AND PRE.[Type] = 2 --prepack content
    /*ISF*/
WHERE SCML.Type NOT IN (
    2
    , 6
    ) -- Item, Itemcharge
