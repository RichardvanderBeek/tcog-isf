SELECT
  -- system information
  SIL.stage_id AS StageID
  , SIL.company_id AS CompanyID
  , SIL.data_connection_id AS DataConnectionID
  , 2 AS SalesValueEntryType
  -- business key
  , SIL.[Document No_] AS DocumentNo
  , SIL.Type AS SalesLineTypeCode
  , 0 AS ItemLedgerEntryNo
  , 0 AS ValueEntryNo
  -- Other fields
  , 0 AS SourceType
  , CAST(NULL AS NVARCHAR(20)) AS SourcePostingGroup
                  , COALESCE(FD.FinancialDimension1ID, 0)   AS FinancialDimension1ID
                  , COALESCE(FD.FinancialDimension2ID, 0)   AS FinancialDimension2ID
                  , COALESCE(FD.FinancialDimension3ID, 0)   AS FinancialDimension3ID
                  , COALESCE(FD.FinancialDimension4ID, 0)   AS FinancialDimension4ID
                  , COALESCE(FD.FinancialDimension5ID, 0)   AS FinancialDimension5ID
                  , COALESCE(FD.FinancialDimension6ID, 0)   AS FinancialDimension6ID
                  , COALESCE(FD.FinancialDimension7ID, 0)   AS FinancialDimension7ID
                  , COALESCE(FD.FinancialDimension8ID, 0)   AS FinancialDimension8ID
  -- dimensions                                                          
  , CAST(NULL AS NVARCHAR(20)) AS ItemCode --comes from Value Entry table
  , CASE 
    WHEN SIL.Type = 1
      THEN SIL.No_
    ELSE ''
    END AS GeneralLedgerAccountCode
  , CASE 
    WHEN SIL.Type = 3
      THEN SIL.No_
    ELSE ''
    END AS ResourceCode
  , CASE 
    WHEN SIL.Type = 4
      THEN SIL.No_
    ELSE ''
    END AS FixedAssetCode
  , CASE 
    WHEN SIL.Type = 5
      THEN SIL.No_
    ELSE ''
    END AS ItemChargeCode
  , SIL.[Posting Date] AS DocumentPostingDate
  , SIH.[Posting Date] AS PostingDate
  , SIH.[Sell-to Customer No_] AS SellToCustomerCode
  , SIH.[Bill-to Customer No_] AS BillToCustomerCode
  , 2 AS DocumentTypeCode -- Invoice
  , SIL.[Location Code] AS LocationCode
  , SIL.[Variant Code] AS ItemVariantCode
  , SIH.[Source Code] AS SourceCode
  , SIL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , SIL.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , CAST(NULL AS NVARCHAR(20)) AS ReturnReasonCode
  , SIH.[Reason Code] AS ReasonCode
  , SIH.[Salesperson Code] AS SalesPersonPurchaserCode
  , SIH.[Currency Code] AS CurrencyCode
  , CAST(NULL AS NVARCHAR(20)) AS InventoryPostingGroupCode
  /* ISF */
  , SIL.[IsfBrand Code] AS BrandCode
  , SIL.[IsfCollection Code] AS CollectionCode
  , SIL.[IsfColor Code] AS ColorCode
  , SIL.[IsfSeason Code] AS SeasonCode
  , SIL.[IsfStyle No_] AS StyleCode
  , SIL.[IsfOrder Type] AS OrderTypeCode
  , SIL.[IsfCup Code] AS CupCode
  , SIL.[IsfWidth Code] AS WidthCode
  , SIL.[IsfLength Code] AS LengthCode
  , SIL.[IsfOrder Origin] AS OrderOriginCode
  , SIL.[IsfQuality Code] AS QualityCode
  , SIL.[IsfPrepack Code] AS PrepackCode
  , SIL.[IsfShipping Window Code] AS ShippingWindowCode
  /* ISF */
  -- measures                                                            
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Quantity (Base)]
    ELSE 0
    END AS InvoicedQuantity -- exclude GL lines
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN SIL.Amount / SIH.[Currency Factor]
    ELSE SIL.Amount
    END AS SalesAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_LCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN SIL.[Line Discount Amount] / SIH.[Currency Factor]
    ELSE SIL.[Line Discount Amount]
    END AS DiscountAmount_LCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Unit Cost (LCY)] * SIL.[Quantity (Base)]
    ELSE 0
    END AS CostAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.Amount / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.Amount * ER.CrossRate
    END AS SalesAmount_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_RCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.[Line Discount Amount] / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.[Line Discount Amount] * ER.CrossRate
    END AS DiscountAmount_RCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Unit Cost] * SIL.[Quantity (Base)] * ER.CrossRate
    ELSE 0
    END AS CostAmount_RCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.Amount / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.Amount * ER.CrossRate
    END AS SalesAmount_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_PCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.[Line Discount Amount] / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.[Line Discount Amount] * ER.CrossRate
    END AS DiscountAmount_PCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN (SIL.[Unit Cost] * SIL.[Quantity (Base)]) * ER.CrossRate
    ELSE 0
    END AS CostAmount_PCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_PCY
  /*ISF*/
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * SIL.[Quantity (Base)])
    ELSE NULL
    END AS PrepackQuantity
/*ISF*/
FROM stage_nav.[Sales Invoice Line] AS SIL
LEFT JOIN stage_nav.[Sales Invoice Header] AS SIH
  ON SIL.[Document No_] = SIH.No_
    AND SIL.company_id = SIH.company_id
    AND SIL.data_connection_id = SIH.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON SIL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = SIL.company_id
    AND ER.DataConnectionID = SIL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON SIL.[Dimension Set ID] = FD.dimensionsetno
    AND SIL.company_id = FD.CompanyID
    AND SIL.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON SIL.No_ = IV.[Item No_]
    AND SIL.[Variant Code] = IV.Code
    AND SIL.Company_Id = IV.Company_Id
    AND SIL.Data_Connection_Id = IV.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = SIL.No_
    AND PRE.[Prepack Code] = SIL.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.Company_Id = SIL.Company_Id
    AND PRE.Data_Connection_Id = SIL.Data_Connection_Id
    AND PRE.[Type] = 2 --prepack content
    /*ISF*/
WHERE SIL.Type NOT IN (
    2
    , 6
    ) -- Item, Itemcharge
