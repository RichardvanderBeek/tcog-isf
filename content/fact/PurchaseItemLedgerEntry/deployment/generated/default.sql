SELECT
  -- system information
  ILE.stage_id AS StageID
  , ILE.Company_Id AS CompanyID
  , ILE.data_connection_id AS DataConnectionID
  -- business key
  , ILE.[Entry No_] AS ItemLedgerEntryNo
  -- dimensions                                                                       
  , ILE.[Item No_] AS ItemCode
  , PILE.DocumentPostingDate AS DocumentPostingDate
  , ILE.[Posting Date] AS PostingDate
  , PILE.BuyFromVendorNo AS BuyFromVendorCode
  , COALESCE(PILE.PayToVendorNo, PILE.BuyFromVendorNo) AS PayToVendorCode
  , ILE.[Document Type] AS DocumentTypeCode
  , ILE.[Document No_] AS DocumentCode
  , ILE.[Location Code] AS LocationCode
  , ILE.[Variant Code] AS ItemVariantCode
  , ILE.[Transport Method] AS TransportMethodCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , ILE.[IsfBrand Code] AS BrandCode
  , ILE.[IsfCollection Code] AS CollectionCode
  , ILE.[IsfColor Code] AS ColorCode
  , ILE.[IsfSeason Code] AS SeasonCode
  , ILE.[IsfStyle No_] AS StyleCode
  , ILE.[IsfOrder Type] AS OrderTypeCode
  , ILE.[IsfCup Code] AS CupCode
  , ILE.[IsfWidth Code] AS WidthCode
  , ILE.[IsfLength] AS LengthCode
  , ILE.[IsfOrder Origin] AS OrderOriginCode
  , ILE.[IsfQuality Code] AS QualityCode
  , ILE.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures                                                                         
  , ILE.[Quantity] AS ReceivedQuantity
  /*ISF*/
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * ILE.Quantity)
    ELSE NULL
    END AS PrepackQuantity
/*ISF*/
FROM stage_nav.[Item Ledger Entry] AS ILE
INNER JOIN help.[PurchaseItemLedgerEntry] AS PILE
  ON ILE.[Entry No_] = PILE.EntryNo
    AND ILE.Company_Id = PILE.Company_ID
    AND ILE.Data_connection_id = PILE.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON ILE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = ILE.company_id
    AND ER.DataConnectionID = ILE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON ILE.[Dimension Set ID] = FD.dimensionsetno
    AND ILE.company_id = FD.companyid
    AND ILE.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON IV.[Item No_] = ILE.[Item No_]
    AND IV.Code = ILE.[Variant Code]
    AND IV.Company_Id = ILE.Company_Id
    AND IV.Data_Connection_Id = ILE.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = ILE.[Item No_]
    AND PRE.[Prepack Code] = ILE.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.[Type] = 2 --prepack content
    AND PRE.Company_Id = ILE.Company_Id
    AND PRE.Data_Connection_Id = ILE.Data_Connection_Id
/*ISF*/
WHERE ILE.[Entry Type] = 0 --Purchase
