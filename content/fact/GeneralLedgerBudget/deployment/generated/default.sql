SELECT
  -- system information
  GLBE.stage_id AS StageID
  , GLBE.Company_Id AS CompanyID
  , GLBE.data_connection_id AS DataConnectionID
  -- dimensions
  , GLBE.[G_L Account No_] AS GeneralLedgerAccountCode
  , GLBE.[Budget Name] AS BudgetModelName
  , GLBE.[Date] AS BudgetDate
  , GLA.Income_Balance AS IsBalanceCode
  , CASE 
    WHEN GLBE.AMOUNT < 0
      THEN 1
    ELSE 0
    END AS IsCreditCode
  , COALESCE(GLA.Income_Balance, - 1) AS IsBalance
  , COALESCE(CASE 
      WHEN GLBE.AMOUNT < 0
        THEN 1
      ELSE 0
      END, - 1) AS IsCredit
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , GLBE.Amount AS Amount_LCY
  -- measures_RCY
  , GLBE.Amount * ER.CrossRate AS Amount_RCY
FROM stage_nav.[G_L Budget Entry] AS GLBE
LEFT JOIN stage_nav.[G_L Account] AS GLA
  ON GLA.No_ = GLBE.[G_L Account No_]
    AND GLA.Company_Id = GLBE.Company_Id
    AND GLA.Data_Connection_Id = GLBE.Data_Connection_Id
LEFT JOIN help.ExchangeRates AS ER
  ON GLBE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = GLBE.company_id
    AND ER.DataConnectionID = GLBE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON GLBE.[Dimension Set ID] = FD.DimensionSetNo
    AND GLBE.company_id = FD.companyID
    AND GLBE.data_connection_id = FD.DataConnectionID
