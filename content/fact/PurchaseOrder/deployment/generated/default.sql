SELECT
  -- system information
  PL.stage_id AS StageID
  , PL.Company_Id AS CompanyID
  , PL.data_connection_id AS DataConnectionID
  -- dimensions                                                                       
  , COALESCE(PL.[Buy-From Vendor No_], PH.[Buy-From Vendor No_]) AS BuyFromVendorCode
  , COALESCE(PL.[Pay-to Vendor No_], PH.[Pay-to Vendor No_]) AS PayToVendorCode
  , PH.[Ship-to Code] AS ShipFromVendorCode
  , PL.[Document No_] AS DocumentCode
  , PL.[Line No_] AS PurchaseOrderLineNo
  , PL.[Document Type] AS DocumentTypeCode
  , PL.[No_] AS ItemCode
  , PL.[Variant Code] AS ItemVariantCode
  , COALESCE(PL.[Location Code], PH.[Location Code]) AS LocationCode
  , PL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(PL.[Gen_ Bus_ Posting Group], PH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , PL.[Unit of Measure] AS UnitOfMeasureCode
  , PH.[Purchaser Code] AS SalesPersonPurchaserCode
  , PH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(PL.[Transport Method], PH.[Transport Method]) AS TransportMethodCode
  , PH.[Order Date] AS OrderDate
  , PH.[Document Date] AS DocumentDate
  , PL.[Expected Receipt Date] AS ExpectedReceiptDate
  , PL.[Promised Receipt Date] AS PromisedReceiptDate
  , PL.[Requested Receipt Date] AS RequestedReceiptDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , PL.[IsfBrand Code] AS BrandCode
  , PL.[IsfCollection Code] AS CollectionCode
  , PL.[IsfColor Code] AS ColorCode
  , PL.[IsfSeason Code] AS SeasonCode
  , PL.[IsfStyle No_] AS StyleCode
  , PL.[IsfOrder Type] AS OrderTypeCode
  , PL.[IsfCup Code] AS CupCode
  , PL.[IsfWidth Code] AS WidthCode
  , PL.[IsfLength Code] AS LengthCode
  , PL.[IsfQuality Code] AS QualityCode
  , PL.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures                                                                         
  , PL.[Gross Weight] AS PurchaseOrderShippedGrossWeight
  , PL.[Net Weight] AS PurchaseOrderShippedNetWeight
  , PL.[Quantity (Base)] AS PurchaseOrderQuantity
  , PL.[Outstanding Qty_ (Base)] AS PurchaseOrderOutstandingQuantity
  , PL.[Qty_ Received (Base)] AS PurchaseOrderReceivedQuantity
  , PL.[Qty_ Invoiced (Base)] AS PurchaseOrderInvoicedQuantity
  , PL.[Qty_ to Invoice (Base)] AS PurchaseOrderToInvoiceQuantity
  , PL.[Qty_ to Receive (Base)] AS PurchaseOrderToReceiveQuantity
  , PL.[Qty_ Rcd_ Not Invoiced (Base)] AS PurchaseOrderReceivedNotInvoicedQuantity
  --LCY measures
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Line Amount] - PL.[Inv_ Discount Amount]) / PH.[Currency Factor]
    ELSE (PL.[Line Amount] - PL.[Inv_ Discount Amount])
    END AS PurchaseOrderAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN PL.[Line Amount] / PH.[Currency Factor]
    ELSE PL.[Line Amount]
    END AS PurchaseOrderGrossAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Outstanding Qty_ (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Outstanding Qty_ (Base)])
    END AS PurchaseOrderOutstandingAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Received (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Qty_ Received (Base)])
    END AS PurchaseOrderReceivedAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Invoiced (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Qty_ Invoiced (Base)])
    END AS PurchaseOrderInvoicedAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ to Invoice (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Qty_ to Invoice (Base)])
    END AS PurchaseOrderToInvoiceAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ to Receive (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Qty_ to Receive (Base)])
    END AS PurchaseOrderToReceiveAmount_LCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Rcd_ Not Invoiced (Base)]) / PH.[Currency Factor]
    ELSE (PL.[Unit Cost] * PL.[Qty_ Rcd_ Not Invoiced (Base)])
    END AS PurchaseOrderReceivedNotInvoicedAmount_LCY
  , PL.[Inv_ Discount Amount] AS PurchaseOrderDiscountAmount_LCY
  -- measures_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Line Amount] - PL.[Inv_ Discount Amount]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Line Amount] - PL.[Inv_ Discount Amount] * ER.CrossRate)
    END AS PurchaseOrderAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN PL.[Line Amount] / PH.[Currency Factor] * ER.CrossRate
    ELSE PL.[Line Amount] * ER.CrossRate
    END AS PurchaseOrderGrossAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Outstanding Qty_ (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Outstanding Qty_ (Base)] * ER.CrossRate)
    END AS PurchaseOrderOutstandingAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Received (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Qty_ Received (Base)] * ER.CrossRate)
    END AS PurchaseOrderReceivedAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Invoiced (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Qty_ Invoiced (Base)] * ER.CrossRate)
    END AS PurchaseOrderInvoicedAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ to Invoice (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Qty_ to Invoice (Base)] * ER.CrossRate)
    END AS PurchaseOrderToInvoiceAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ to Receive (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Qty_ to Receive (Base)] * ER.CrossRate)
    END AS PurchaseOrderToReceiveAmount_RCY
  , CASE 
    WHEN PH.[Currency Factor] <> 0
      THEN (PL.[Unit Cost] * PL.[Qty_ Rcd_ Not Invoiced (Base)]) / PH.[Currency Factor] * ER.CrossRate
    ELSE (PL.[Unit Cost] * PL.[Qty_ Rcd_ Not Invoiced (Base)] * ER.CrossRate)
    END AS PurchaseOrderReceivedNotInvoicedAmount_RCY
  , PL.[Inv_ Discount Amount] * ER.CrossRate AS PurchaseOrderDiscountAmount_RCY
FROM stage_nav.[Purchase Line] AS PL
LEFT JOIN stage_nav.[Purchase Header] AS PH
  ON PL.[Document Type] = PH.[Document Type]
    AND PL.[Document No_] = PH.[No_]
    AND PL.Company_Id = PH.Company_ID
    AND PL.Data_connection_id = PH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PL.[Order Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PL.company_id
    AND ER.DataConnectionID = PL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON PL.[Dimension Set ID] = FD.dimensionsetno
    AND PL.company_id = FD.companyid
    AND PL.data_connection_id = FD.DataConnectionID
WHERE PL.[Document Type] IN (
    0
    , 1
    , 4
    )
