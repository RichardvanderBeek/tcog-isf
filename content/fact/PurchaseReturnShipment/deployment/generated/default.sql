SELECT
  -- system information
  RSL.stage_id AS StageID
  , RSL.Company_Id AS CompanyID
  , RSL.data_connection_id AS DataConnectionID
  -- dimensions
  , RSH.No_ AS DocumentNo
  , 7 AS DocumentTypeCode --Return Receipt
  , COALESCE(RSL.[Buy-From Vendor No_], RSH.[Buy-From Vendor No_]) AS BuyFromVendorCode
  , COALESCE(RSL.[Pay-to Vendor No_], RSL.[Pay-to Vendor No_]) AS PayToVendorCode
  , RSL.[No_] AS ItemCode
  , RSL.[Variant Code] AS ItemVariantCode
  , RSH.[Source Code] AS SourceCode
  , COALESCE(RSL.[Location Code], RSH.[Location Code]) AS LocationCode
  , RSL.[Return Reason Code] AS ReturnReasonCode
  , RSL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(RSL.[Gen_ Bus_ Posting Group], RSH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , RSH.[Reason Code] AS ReasonCode
  , RSH.[Purchaser Code] AS SalesPersonPurchaserCode
  , RSH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(RSL.[Transport Method], RSH.[Transport Method]) AS TransportMethodCode
  , RSL.[Posting Date] AS PostingDate
  , RSH.[Expected Receipt Date] AS ExpectedReceiptDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , RSL.[IsfBrand Code] AS BrandCode
  , RSL.[IsfCollection Code] AS CollectionCode
  , RSL.[IsfColor Code] AS ColorCode
  , RSL.[IsfSeason Code] AS SeasonCode
  , RSL.[IsfStyle No_] AS StyleCode
  , RSL.[IsfOrder Type] AS OrderTypeCode
  , RSL.[IsfCup Code] AS CupCode
  , RSL.[IsfWidth Code] AS WidthCode
  , RSL.[IsfLength] AS LengthCode
  , RSL.[IsfQuality Code] AS QualityCode
  , RSL.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures                                                                                              
  , RSL.[Quantity (Base)] AS ShippedQuantity
  , RSL.[Qty_ Invoiced (Base)] AS InvoicedQuantity
  , RSL.[Gross Weight] AS ShippedGrossWeight
  , RSL.[Net Weight] AS ShippedNetWeight
  -- measures_LCY                                                                                                        
  , RSL.[Quantity (Base)] * RSL.[Unit Price (LCY)] * ((100 - RSL.[Line Discount _]) / 100) AS ShippedAmount_LCY
  -- measures_RCY
  , RSL.[Quantity (Base)] * RSL.[Unit Price (LCY)] * ((100 - RSL.[Line Discount _]) / 100) * ER.CrossRate AS ShippedAmount_RCY
  /* ISF */
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * RSL.[Quantity (Base)])
    ELSE NULL
    END AS PrepackShippedQuantity
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * RSL.[Qty_ Invoiced (Base)])
    ELSE NULL
    END AS PrepackInvoicedQuantity
/* ISF */
FROM stage_nav.[Return Shipment Line] AS RSL
LEFT JOIN stage_nav.[Return Shipment Header] AS RSH
  ON RSL.[Buy-From Vendor No_] = RSH.[Buy-From Vendor No_]
    AND RSL.[Document No_] = RSH.[No_]
    AND RSL.Company_Id = RSH.Company_ID
    AND RSL.Data_connection_id = RSH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON RSL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = RSL.company_id
    AND ER.DataConnectionID = RSL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON RSL.[Dimension Set ID] = FD.dimensionsetno
    AND RSL.company_id = FD.companyid
    AND RSL.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON IV.[Item No_] = RSL.[No_]
    AND IV.Code = RSL.[Variant Code]
    AND IV.Company_Id = RSL.Company_Id
    AND IV.Data_Connection_Id = RSL.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = RSL.[No_]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.[Prepack Code] = RSL.[Isfprepack Code]
    AND PRE.[Type] = 2 --prepack content
    AND PRE.Company_Id = RSL.Company_Id
    AND PRE.Data_Connection_Id = RSL.Data_Connection_Id
    /*ISF*/
