SELECT
  -- system information
  IBE.stage_id AS StageID
  , IBE.Company_Id AS CompanyID
  , IBE.data_connection_id AS DataConnectionID
  -- dimensions
  , IBE.[Entry No_] AS EntryNo
  , IBE.[Item No_] AS ItemCode
  , IBE.[Location Code] AS LocationCode
  , IBE.[Budget Name] AS SalesBudgetCode
  , IBE.[Date] AS SalesBudgetDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , IBE.[Cost Amount] AS SalesBudgetCostAmount_LCY
  , IBE.[Sales Amount] AS SalesBudgetAmount_LCY
  -- measures
  , IBE.Quantity AS SalesBudgetQuantity
  -- measures_RCY
  , IBE.[Cost Amount] * ER.CrossRate AS SalesBudgetCostAmount_RCY
  , IBE.[Sales Amount] * ER.CrossRate AS SalesBudgetAmount_RCY
FROM stage_nav.[Item Budget Entry] AS IBE
LEFT JOIN help.ExchangeRates AS ER
  ON IBE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = IBE.company_id
    AND ER.DataConnectionID = IBE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON IBE.[Dimension Set ID] = FD.DimensionSetNo
    AND IBE.company_id = FD.CompanyID
    AND IBE.data_connection_id = FD.DataConnectionID
WHERE IBE.[Analysis Area] = 0 --Sales
