SELECT
  -- system information
  FLE.stage_id AS StageID
  , FLE.Company_Id AS CompanyID
  , FLE.data_connection_id AS DataConnectionID
  -- dimensions    
  , FLE.[Entry No_] AS EntryCode
  , FLE.[G_L Entry No_] AS GLEntryCode
  , FLE.[FA No_] AS FixedAssetCode
  , CAST(CONVERT(NVARCHAR(255), FLE.[FA Posting Date], 112) AS DATETIME) AS FAPostingDate
  , CAST(CONVERT(NVARCHAR(255), FLE.[Posting Date], 112) AS DATETIME) AS PostingDate
  , FLE.[Depreciation Book Code] AS DepreciationBookCode
  , FLE.[FA Posting Category] AS FAPostingCategory
  , FLE.[FA Posting Type] AS FAPostingType
  , FLE.[Part of Book Value] AS PartofBookValue
  , FLE.[Source Code] AS SourceCode
  , FLE.[Reason Code] AS ReasonCode
  , FLE.[FA Posting Group] AS FAPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- Measures LCY                        
  , FLE.Amount AS Amount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 0
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS AcquisitionCostAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 1
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS DepreciationAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 2
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS WriteDownAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 3
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS AppreciationAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 4
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS Custom1Amount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 5
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS Custom2Amount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 6
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS ProceedsOnDisposalAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 7
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS SalvageValueAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 8
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS GainLossAmount_LCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 9
      THEN FLE.[Amount (LCY)]
    ELSE 0
    END AS BookValueOnDisposalAmount_LCY
  -- Measures RCY
  , FLE.Amount * ER.CrossRate AS Amount_RCY
  , FLE.[Debit Amount] * ER.CrossRate AS Debit_RCY
  , FLE.[Credit Amount] * ER.CrossRate AS Credit_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 0
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS AcquisitionCostAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 1
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS DepreciationAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 2
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS WriteDownAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 3
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS AppreciationAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 4
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS Custom1Amount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 5
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS Custom2Amount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 6
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS ProceedsOnDisposalAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 7
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS SalvageValueAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 8
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS GainLossAmount_RCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 9
      THEN FLE.Amount * ER.CrossRate
    ELSE 0
    END AS BookValueOnDisposalAmount_RCY
  -- Measures PCY
  , FLE.Amount AS Amount_PCY
  , FLE.[Debit Amount] AS DebitAmount_PCY
  , FLE.[Credit Amount] AS Credit_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 0
      THEN FLE.[Amount]
    ELSE 0
    END AS AcquisitionCostAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 1
      THEN FLE.[Amount]
    ELSE 0
    END AS DepreciationAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 2
      THEN FLE.[Amount]
    ELSE 0
    END AS WriteDownAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 3
      THEN FLE.[Amount]
    ELSE 0
    END AS AppreciationAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 4
      THEN FLE.[Amount]
    ELSE 0
    END AS Custom1Amount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 5
      THEN FLE.[Amount]
    ELSE 0
    END AS Custom2Amount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 6
      THEN FLE.[Amount]
    ELSE 0
    END AS ProceedsOnDisposalAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 7
      THEN FLE.[Amount]
    ELSE 0
    END AS SalvageValueAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 8
      THEN FLE.[Amount]
    ELSE 0
    END AS GainLossAmount_PCY
  , CASE 
    WHEN FLE.[FA Posting Type] = 9
      THEN FLE.[Amount]
    ELSE 0
    END AS BookValueOnDisposalAmount_PCY
FROM stage_nav.[FA Ledger Entry] AS FLE
LEFT JOIN help.ExchangeRates AS ER
  ON FLE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = FLE.company_id
    AND ER.DataConnectionID = FLE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON FLE.[Dimension Set ID] = FD.DimensionSetNo
    AND FLE.company_id = FD.CompanyID
    AND FLE.data_connection_id = FD.DataConnectionID
