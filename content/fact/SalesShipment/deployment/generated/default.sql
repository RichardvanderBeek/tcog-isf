SELECT
  -- system information
  SSHL.stage_id AS StageID
  , SSHL.Company_Id AS CompanyID
  , SSHL.data_connection_id AS DataConnectionID
  -- dimensions                     
  , SSH.No_ AS DocumentSalesCode
  , 5 AS DocumentSalesTypeCode --Sales Shipment
  , COALESCE(SSHL.[Sell-to Customer No_], SSH.[Sell-to Customer No_]) AS SellToCustomerCode
  , COALESCE(SSHL.[Bill-to Customer No_], SSHL.[Sell-to Customer No_]) AS BillToCustomerCode
  , SSHL.[Document No_] AS SalesShipmentDocumentNo
  , SSHL.[No_] AS ItemCode
  , SSHL.[Variant Code] AS ItemVariantCode
  , SSH.[Source Code] AS SourceCode
  , COALESCE(SSHL.[Location Code], SSH.[Location Code]) AS LocationCode
  , SSHL.[Return Reason Code] AS ReturnReasonCode
  , SSHL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(SSHL.[Gen_ Bus_ Posting Group], SSH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , SSHL.[Unit of Measure Code] AS UnitOfMeasureCode
  , SSH.[Shipping Agent Code] AS ShippingAgentCode
  , SSH.[Reason Code] AS ReasonCode
  , SSH.[Shipping Agent Service Code] AS ShippingAgentServicesCode
  , SSH.[Salesperson Code] AS SalesPersonPurchaserCode
  , SSH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(SSHL.[Transport Method], SSH.[Transport Method]) AS TransportMethodCode
  , SSH.[Order No_] AS DocumentCode
  , 1 AS DocumentTypeCode -- Sales Order
  , SSHL.[Posting Date] AS PostingDate
  , COALESCE(SSHL.[Shipment Date], SSH.[Shipment Date]) AS ShipmentDate
  , COALESCE(SSHL.[Requested Delivery Date], SSH.[Requested Delivery Date]) AS RequestedDeliveryDate
  , SSHL.[Planned Delivery Date] AS PlannedDeliveryDate
  , COALESCE(SSHL.[Promised Delivery Date], SSH.[Promised Delivery Date]) AS PromisedDeliveryDate
  , SSHL.[Planned Shipment Date] AS PlannedShipmentDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , SSHL.[IsfBrand Code] AS BrandCode
  , SSHL.[IsfCollection Code] AS CollectionCode
  , SSHL.[IsfColor Code] AS ColorCode
  , SSHL.[IsfSeason Code] AS SeasonCode
  , SSHL.[IsfStyle No_] AS StyleCode
  , SSHL.[IsfOrder Type] AS OrderTypeCode
  , SSHL.[IsfCup Code] AS CupCode
  , SSHL.[IsfWidth Code] AS WidthCode
  , SSHL.[IsfLength Code] AS LengthCode
  , SSHL.[IsfOrder Origin] AS OrderOriginCode
  , SSHL.[IsfQuality Code] AS QualityCode
  , SSHL.[IsfPrepack Code] AS PrepackCode
  , SSHL.[IsfShipping Window Code] AS ShippingWindowCode
  /* ISF */
  -- measures                                                                                           
  , SSHL.[Quantity (Base)] AS ShippedQuantity
  , SSHL.[Qty_ Invoiced (Base)] AS InvoicedQuantity
  , SSHL.[Qty_ Shipped Not Invoiced] AS ShippedNotInvoicedQuantity
  , SSHL.[Gross Weight] AS ShippedGrossWeight
  , SSHL.[Net Weight] AS ShippedNetWeight
  , SSHL.[Quantity (Base)] * SSHL.[Unit Price] * ((100 - SSHL.[Line Discount _]) / 100) AS ShippedAmount_LCY
  -- measures_RCY
  , SSHL.[Quantity (Base)] * SSHL.[Unit Price] * ((100 - SSHL.[Line Discount _]) / 100) * ER.CrossRate AS ShippedAmount_RCY
  /* ISF */
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * SSHL.[Quantity (Base)])
    ELSE NULL
    END AS PrepackShippedQuantity
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * SSHL.[Qty_ Invoiced (Base)])
    ELSE NULL
    END AS PrepackInvoicedQuantity
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * SSHL.[Qty_ Shipped Not Invoiced])
    ELSE NULL
    END AS PrepackShippedNotInvoicedQuantity
/* ISF */
FROM stage_nav.[Sales Shipment Line] AS SSHL
LEFT JOIN stage_nav.[Sales Shipment Header] AS SSH
  ON SSHL.[Sell-to Customer No_] = SSH.[Sell-to Customer No_]
    AND SSHL.[Document No_] = SSH.[No_]
    AND SSHL.Company_Id = SSH.Company_ID
    AND SSHL.Data_connection_id = SSH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON SSHL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = SSHL.company_id
    AND ER.DataConnectionID = SSHL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON SSHL.[Dimension Set ID] = FD.dimensionsetno
    AND SSHL.company_id = FD.companyid
    AND SSHL.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON SSHL.No_ = IV.[Item No_]
    AND SSHL.[Variant Code] = IV.Code
    AND SSHL.Company_Id = IV.Company_Id
    AND SSHL.Data_Connection_Id = IV.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = SSHL.No_
    AND PRE.[Prepack Code] = SSHL.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.Company_Id = SSHL.Company_Id
    AND PRE.Data_Connection_Id = SSHL.Data_Connection_Id
    AND PRE.[Type] = 2 --prepack content
    /*ISF*/
