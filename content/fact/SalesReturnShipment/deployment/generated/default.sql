SELECT
  -- system information
  RRL.stage_id AS StageID
  , RRL.Company_Id AS CompanyID
  , RRL.data_connection_id AS DataConnectionID
  -- dimensions
  , RRH.No_ AS DocumentNo
  , 5 AS DocumentTypeCode --Return Shipment
  , COALESCE(RRL.[Sell-to Customer No_], RRH.[Sell-to Customer No_]) AS SellToCustomerCode
  , COALESCE(RRL.[Bill-to Customer No_], RRL.[Sell-to Customer No_]) AS BillToCustomerCode
  , RRL.[No_] AS ItemCode
  , RRL.[Variant Code] AS ItemVariantCode
  , RRH.[Source Code] AS SourceCode
  , COALESCE(RRL.[Location Code], RRH.[Location Code]) AS LocationCode
  , RRL.[Return Reason Code] AS ReturnReasonCode
  , RRL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(RRL.[Gen_ Bus_ Posting Group], RRH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , RRL.[Unit of Measure Code] AS UnitOfMeasureCode
  , RRH.[Shipping Agent Code] AS ShippingAgentCode
  , RRH.[Reason Code] AS ReasonCode
  , RRH.[Salesperson Code] AS SalesPersonPurchaserCode
  , RRH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(RRL.[Transport Method], RRH.[Transport Method]) AS TransportMethodCode
  , RRL.[Posting Date] AS PostingDate
  , COALESCE(RRL.[Shipment Date], RRH.[Shipment Date]) AS ShipmentDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  /* ISF */
  , RRL.[IsfBrand Code] AS BrandCode
  , RRL.[IsfCollection Code] AS CollectionCode
  , RRL.[IsfColor Code] AS ColorCode
  , RRL.[IsfSeason Code] AS SeasonCode
  , RRL.[IsfStyle No_] AS StyleCode
  , RRL.[IsfOrder Type] AS OrderTypeCode
  , RRL.[IsfCup Code] AS CupCode
  , RRL.[IsfWidth Code] AS WidthCode
  , RRL.[IsfLength] AS LengthCode
  , RRL.[IsfOrder Origin] AS OrderOriginCode
  , RRL.[IsfQuality Code] AS QualityCode
  , RRL.[IsfPrepack Code] AS PrepackCode
  /* ISF */
  -- measures                                                                                          
  , RRL.[Quantity (Base)] AS ShippedQuantity
  , RRL.[Qty_ Invoiced (Base)] AS InvoicedQuantity
  , RRL.[Gross Weight] AS ShippedGrossWeight
  , RRL.[Net Weight] AS ShippedNetWeight
  , RRL.[Quantity (Base)] * RRL.[Unit Price] * ((100 - RRL.[Line Discount _]) / 100) AS ShippedAmount_LCY
  -- measures_RCY
  , RRL.[Quantity (Base)] * RRL.[Unit Price] * ((100 - RRL.[Line Discount _]) / 100) * ER.CrossRate AS ShippedAmount_RCY
  /* ISF */
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * RRL.[Quantity (Base)])
    ELSE NULL
    END AS PrepackShippedQuantity
  , CASE 
    WHEN PRE.Quantity IS NOT NULL
      THEN (PRE.Quantity * RRL.[Qty_ Invoiced (Base)])
    ELSE NULL
    END AS PrepackInvoicedQuantity
/* ISF */
FROM stage_nav.[Return Receipt Line] AS RRL
LEFT JOIN stage_nav.[Return Receipt Header] AS RRH
  ON RRL.[Sell-to Customer No_] = RRH.[Sell-to Customer No_]
    AND RRL.[Document No_] = RRH.[No_]
    AND RRL.Company_Id = RRH.Company_ID
    AND RRL.Data_connection_id = RRH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON RRL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = RRL.company_id
    AND ER.DataConnectionID = RRL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON RRL.[Dimension Set ID] = FD.dimensionsetno
    AND RRL.company_id = FD.companyid
    AND RRL.data_connection_id = FD.DataConnectionID
/*ISF*/
LEFT JOIN stage_nav.[Item Variant] AS IV
  ON RRL.[No_] = IV.[Item No_]
    AND RRL.[Variant Code] = IV.Code
    AND RRL.Company_Id = IV.Company_Id
    AND RRL.Data_Connection_Id = IV.Data_Connection_Id
LEFT JOIN stage_nav.[IsfPrepack Content] AS PRE
  ON PRE.[No_] = RRL.[No_]
    AND PRE.[Prepack Code] = RRL.[Isfprepack Code]
    AND PRE.[Size Code] = IV.Isfsize
    AND PRE.Company_Id = RRL.Company_Id
    AND PRE.Data_Connection_Id = RRL.Data_Connection_Id
    AND PRE.[Type] = 2 --prepack content
    /*ISF*/
