SELECT
  -- system information
  CFFE.stage_id AS StageID
  , CFFE.company_id AS CompanyID
  , CFFE.data_connection_id AS DataConnectionID
  -- dimensions  
  , CFFE.[G_L Budget Name] AS BudgetModelName
  , CFFE.[Document No_] AS CashflowDocumentCode
  , CFFE.[Entry No_] AS CashflowDocumentEntryNo
  , CFFE.[Cash Flow Account No_] AS CashflowAccountCode
  , CFFE.[Cash Flow Forecast No_] AS CashflowForecastCode
  , CFFE.[Source No_] AS CashflowForecastSourceCode
  , CFFE.[Cash Flow Date] AS CashflowForecastDate
  , CFFE.[Source Type] AS CashflowSourceTypeCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures   
  , CFFE.[Amount (LCY)] AS Amount_LCY
  , CFFE.[Payment Discount] AS PaymentDiscountAmount_LCY
  -- measures_RCY
  , CFFE.[Amount (LCY)] * ER.CrossRate AS Amount_RCY
  , CFFE.[Payment Discount] * ER.CrossRate AS PaymentDiscountAmount_RCY
FROM stage_nav.[Cash Flow Forecast Entry] AS CFFE
LEFT JOIN help.ExchangeRates AS ER
  ON CFFE.[Cash Flow Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = CFFE.company_id
    AND ER.DataConnectionID = CFFE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON CFFE.[Dimension Set ID] = FD.DimensionSetNo
    AND CFFE.company_id = FD.CompanyID
    AND CFFE.data_connection_id = FD.DataConnectionID
