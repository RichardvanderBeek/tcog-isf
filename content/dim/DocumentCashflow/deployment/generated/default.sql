SELECT
  -- system information
  CFE.company_id AS CompanyID
  , CFE.data_connection_id AS DataConnectionID
  , CFE.stage_id AS StageID
  -- business key                     
  , CFE.[Document No_] AS CashflowDocumentCode
  , CFE.[Entry No_] AS CashflowDocumentEntryNo
  , CFE.[Source Type] AS CashflowSourceTypeCode
  , COALESCE(e_DT.OptionValue, 'N/A') AS CashflowSourceTypeDescription
  , COALESCE(NULLIF(CFE.[Cash Flow Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS CashflowDate
FROM stage_nav.[Cash Flow Forecast Entry] AS CFE
LEFT JOIN help.Enumerations AS e_DT
  ON e_DT.OptionNo = CFE.[Source Type]
    AND e_DT.ColumnName = 'Cashflow Forecast Entry'
    AND e_DT.TableName = 'Source Type'
    AND e_DT.dataconnectionid = CFE.data_connection_id
GROUP BY CFE.company_id
  , CFE.data_connection_id
  , CFE.stage_id
  , CFE.[Document No_]
  , CFE.[Entry No_]
  , CFE.[Source Type]
  , e_DT.OptionValue
  , CFE.[Cash Flow Date]
