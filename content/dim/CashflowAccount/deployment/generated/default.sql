SELECT
  -- system information
  CFA.stage_id AS StageID
  , CFA.company_id AS CompanyID
  , CFA.data_connection_id AS DataConnectionID
  -- business key                                                               
  , CFA.[No_] AS CashflowAccountCode
  -- attributes                                                         
  , COALESCE(NULLIF(CFA.[Name], ''), 'N/A') AS CashflowAccountDesc
  , COALESCE(NULLIF(CFA.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CFA.[Name], ''), 'N/A') AS CashflowAccountCodeDesc
  , COALESCE(NULLIF(CFA.[Search Name], ''), 'N/A') AS CashflowAccountSearchName
  , CFA.[Account Type] AS CashflowAccountTypeCode
  , COALESCE(NULLIF(e_at.OptionValue, ''), 'N/A') AS CashflowAccountTypeDesc
  , CFA.[Source Type] AS CashflowAccountSourceTypeCode
  , COALESCE(NULLIF(e_st.OptionValue, ''), 'N/A') AS CashflowAccountSourceTypeDesc
FROM stage_nav.[Cash Flow Account] AS CFA
LEFT JOIN help.Enumerations AS e_at
  ON e_at.OptionNo = CFA.[Account Type]
    AND e_at.ColumnName = 'Account Type'
    AND e_at.TableName = 'Cash Flow Account'
    AND e_at.DataConnectionId = CFA.data_connection_id
LEFT JOIN help.Enumerations AS e_st
  ON e_st.OptionNo = CFA.[Source Type]
    AND e_st.ColumnName = 'Source Type'
    AND e_st.TableName = 'Cash Flow Account'
    AND e_st.DataConnectionId = CFA.data_connection_id
