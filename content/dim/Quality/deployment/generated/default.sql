SELECT
  -- system information
  QUAL.stage_id AS StageID
  , QUAL.company_id AS CompanyID
  , QUAL.data_connection_id AS DataConnectionID
  -- business key
  , QUAL.Code AS QualityCode
  --  attributes                        
  , COALESCE(NULLIF(QUAL.Description, ''), 'N/A') AS QualityDescription
  , COALESCE(NULLIF(QUAL.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(QUAL.Description, ''), 'N/A') AS QualityCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(QUAL.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), QUAL.Company_Id) + + ' - ' + QUAL.Code AS QualitySortingCode
FROM stage_nav.[IsfAttribute] AS QUAL
WHERE QUAL.Type = 8 -- Quality
