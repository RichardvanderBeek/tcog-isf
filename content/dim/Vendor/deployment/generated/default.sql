SELECT
  -- system information
  V.stage_id AS StageID
  , V.company_id AS CompanyID
  , V.data_connection_id AS DataConnectionID
  -- business key
  , V.No_ AS VendorCode
  --  attributes                        
  , COALESCE(NULLIF(V.NAME, ''), 'N/A') AS VendorName
  , COALESCE(NULLIF(V.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(V.NAME, ''), 'N/A') AS VendorCodeName
  , COALESCE(NULLIF(V.[Country_Region Code], ''), 'N/A') AS VendorCountryCode
  , COALESCE(NULLIF(CO.NAME, ''), 'N/A') AS VendorCountryDescription
  , COALESCE(NULLIF(V.[Country_Region Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CO.NAME, ''), 'N/A') AS VendorCountryCodeDescription
  , COALESCE(NULLIF(V.[Territory Code], ''), 'N/A') AS VendorTerritoryCode
  , COALESCE(NULLIF(T.NAME, ''), 'N/A') AS VendorTerritoryDescription
  , COALESCE(NULLIF(V.[Territory Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(T.NAME, ''), 'N/A') AS VendorTerritoryCodeDescription
  , COALESCE(NULLIF(V.City, ''), 'N/A') AS VendorCity
  , COALESCE(NULLIF(V.[Vendor Posting Group], ''), 'N/A') AS VendorPostingGroupCode
  , COALESCE(NULLIF(V.[Vendor Posting Group], ''), 'N/A') AS VendorPostingGroupDescription
  , COALESCE(NULLIF(V.[Vendor Posting Group], ''), 'N/A') AS VendorPostingGroupCodeDescription
  , COALESCE(NULLIF(V.[Gen_ Bus_ Posting Group], ''), 'N/A') AS GeneralBusinessPostingGroupCode
  , COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupDescription
  , COALESCE(NULLIF(V.[Gen_ Bus_ Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupCodeDescription
  , COALESCE(NULLIF(V.[Purchaser Code], ''), 'N/A') AS PurchasepersonCode
  , COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS PurchasepersonName
  , COALESCE(NULLIF(V.[Purchaser Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS PurchasepersonCodeName
  , COALESCE(NULLIF(V.[Payment Terms Code], ''), 'N/A') AS PaymentTermsCode
  , COALESCE(NULLIF(PT.Description, ''), 'N/A') AS PaymentTermsDescription
  , COALESCE(NULLIF(V.[Payment Terms Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PT.Description, ''), 'N/A') AS PaymentTermsCodeDescription
  , COALESCE(NULLIF(V.[Currency Code], ''), 'N/A') AS CurrencyCode
  , COALESCE(NULLIF(CU.Description, ''), 'N/A') AS CurrencyDescription
  , COALESCE(NULLIF(V.[Currency Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CU.Description, ''), 'N/A') AS CurrencyCodeDescription
FROM stage_nav.Vendor AS V
LEFT JOIN stage_nav.Country_Region AS CO
  ON CO.Code = V.[Country_Region Code]
    AND CO.company_id = V.company_id
    AND CO.data_connection_id = V.data_connection_id
LEFT JOIN stage_nav.Territory AS T
  ON T.Code = V.[Territory Code]
    AND T.company_id = V.company_id
    AND T.data_connection_id = V.data_connection_id
LEFT JOIN stage_nav.Salesperson_Purchaser AS SP
  ON SP.Code = V.[Purchaser Code]
    AND SP.company_id = V.company_id
    AND SP.data_connection_id = V.data_connection_id
LEFT JOIN stage_nav.[Payment Terms] AS PT
  ON PT.Code = V.[Payment Terms Code]
    AND PT.company_id = V.company_id
    AND PT.data_connection_id = V.data_connection_id
LEFT JOIN stage_nav.Currency AS CU
  ON CU.Code = V.[Currency Code]
    AND CU.company_id = V.company_id
    AND CU.data_connection_id = V.data_connection_id
LEFT JOIN stage_nav.[Gen_ Business Posting Group] AS GBPG
  ON GBPG.Code = V.[Gen_ Bus_ Posting Group]
    AND GBPG.company_id = V.company_id
    AND GBPG.data_connection_id = V.data_connection_id
