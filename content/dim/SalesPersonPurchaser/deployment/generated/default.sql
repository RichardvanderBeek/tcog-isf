SELECT
  -- System information
  SP.stage_id AS StageID
  , SP.company_id AS CompanyID
  , SP.data_connection_id AS DataConnectionID
  -- Business key
  , SP.Code AS SalespersonPurchaserCode
  -- Attributes
  , COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS SalespersonPurchaserName
  , COALESCE(NULLIF(SP.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS SalespersonPurchaserCodeName
FROM stage_nav.Salesperson_Purchaser AS SP
