SELECT
  -- System Information
  WE.stage_id AS StageID
  , WE.company_id AS CompanyID
  , WE.data_connection_id AS DataConnectionID
  -- Business Key                                                      
  , WE.[User ID] AS WarehouseEmployeeCode
  -- Attributes
  , COALESCE(ISNULL(U.[Full Name], ''), 'N/A') AS WarehouseEmployeeName
  , COALESCE(ISNULL(L.[Code], ''), 'N/A') AS LocationCode
  , COALESCE(ISNULL(L.[Name], ''), 'N/A') AS LocationName
  , COALESCE(ISNULL(L.[Code], ''), 'N/A') + ' - ' + COALESCE(ISNULL(L.[Name], ''), 'N/A') AS LocationCodeName
FROM stage_nav.[Warehouse Employee] AS WE
LEFT JOIN stage_nav.[User] AS U
  ON U.[User Name] = WE.[User ID]
    AND U.data_connection_id = WE.data_connection_id
LEFT JOIN stage_nav.Location AS L
  ON L.Code = WE.[Location Code]
    AND L.company_id = WE.company_id
    AND L.data_connection_id = WE.data_connection_id
