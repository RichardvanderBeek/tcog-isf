SELECT
  -- system information
  PRE.stage_id AS StageID
  , PRE.company_id AS CompanyID
  , PRE.data_connection_id AS DataConnectionID
  -- business key                                                                                           
  , PRE.[No_] AS ItemCode
  , COALESCE(NULLIF(PRE.[Prepack Code], ''), 'N/A') AS PrepackCode
  --  attributes                        
  , COALESCE(NULLIF(PRE.Description, ''), 'N/A') AS PrepackDescription
  , COALESCE(NULLIF(PRE.[Prepack Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PRE.Description, ''), 'N/A') AS PrepackCodeDescription
FROM stage_nav.[IsfPrepack Header] AS PRE
WHERE PRE.[Type] = 1 --PrepackCode
