SELECT
  -- system information
  IT.stage_id AS StageID
  , IT.company_id AS CompanyID
  , IT.data_connection_id AS DataConnectionID
  -- business key
  , IT.No_ AS ItemCode
  --  attributes                        
  , COALESCE(NULLIF(IT.Description, ''), 'N/A') AS ItemDescription
  , COALESCE(NULLIF(IT.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IT.Description, ''), 'N/A') AS ItemCodeDescription
  , COALESCE(NULLIF(IT.[Base Unit of Measure], ''), 'N/A') AS ItemUnitOfMeasureCode
  , COALESCE(NULLIF(UOM.Description, ''), 'N/A') AS ItemUnitOfMeasureDescription
  , COALESCE(NULLIF(IT.[Base Unit of Measure], ''), 'N/A') + ' - ' + COALESCE(NULLIF(UOM.Description, ''), 'N/A') AS ItemUnitOfMeasureCodeDescription
  , COALESCE(NULLIF(IT.[Inventory Posting Group], ''), 'N/A') AS ItemInventoryPostingGroupCode
  , COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS ItemInventoryPostingGroupDescription
  , COALESCE(NULLIF(IT.[Inventory Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS ItemInventoryPostingGroupCodeDescription
  , COALESCE(NULLIF(IT.[Item Disc_ Group], ''), 'N/A') AS ItemDiscountGroupCode
  , COALESCE(NULLIF(IDG.Description, ''), 'N/A') AS ItemDiscountGroupDescription
  , COALESCE(NULLIF(IT.[Item Disc_ Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IDG.Description, ''), 'N/A') AS ItemDiscountGroupCodeDescription
  , COALESCE(NULLIF(IT.[Vendor No_], ''), 'N/A') AS ItemVendorCode
  , COALESCE(NULLIF(V.[Name], ''), 'N/A') AS ItemVendorName
  , COALESCE(NULLIF(IT.[Vendor No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(V.NAME, ''), 'N/A') AS ItemVendorCodeName
  , COALESCE(NULLIF(IT.[Gen_ Prod_ Posting Group], ''), 'N/A') AS ItemGenProdPostingGroupCode
  , COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS ItemGenProdPostingGroupDescription
  , COALESCE(NULLIF(IT.[Gen_ Prod_ Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS ItemGenProdPostingGroupCodeDescription
  , COALESCE(NULLIF(IT.[Item Category Code], ''), 'N/A') AS ItemCategoryCode
  , COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemCategoryDescription
  , COALESCE(NULLIF(IT.[Item Category Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemCategoryCodeDescription
  , COALESCE(NULLIF(IT.[Product Group Code], ''), 'N/A') AS ItemProductGroupCode
  , COALESCE(NULLIF(PG.Description, ''), 'N/A') AS ItemProductGroupDescription
  , COALESCE(NULLIF(IT.[Product Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PG.Description, ''), 'N/A') AS ItemProductGroupCodeDescription
  , CAST(CAST(IT.[Unit Cost] AS DECIMAL(17,2)) AS NVARCHAR(255)) AS ItemUnitCost
  /* ISF */
  , COALESCE(NULLIF(IT.[IsfCollection Code], ''), 'N/A') AS CollectionCode
  , COALESCE(NULLIF(COLC.Description, ''), 'N/A') AS CollectionDescription
  , COALESCE(NULLIF(IT.[IsfCollection Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(COLC.Description, ''), 'N/A') AS CollectionCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(COLC.Sorting, 0)) + ' - ' + COLC.Code, 'N/A') AS CollectionSortingCode
  , COALESCE(NULLIF(IT.[IsfSeason Code], ''), 'N/A') AS SeasonCode
  , COALESCE(NULLIF(SC.Description, ''), 'N/A') AS SeasonDescription
  , COALESCE(NULLIF(IT.[IsfSeason Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(SC.Description, ''), 'N/A') AS SeasonCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(SC.Sorting, 0)) + ' - ' + SC.Code, 'N/A') AS SeasonSortingCode
  , COALESCE(NULLIF(IT.[IsfBrand Code], ''), 'N/A') AS BrandCode
  , COALESCE(NULLIF(BC.Description, ''), 'N/A') AS BrandDescription
  , COALESCE(NULLIF(IT.[IsfBrand Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(BC.Description, ''), 'N/A') AS BrandCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(BC.Sorting, 0)) + ' - ' + BC.Code, 'N/A') AS BrandSortingCode
  , COALESCE(NULLIF(IT.[IsfColor Code], ''), 'N/A') AS ColorCode
  , COALESCE(NULLIF(CC.Description, ''), 'N/A') AS ColorDescription
  , COALESCE(NULLIF(IT.[IsfColor Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CC.Description, ''), 'N/A') AS ColorCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(CC.Sorting, 0)) + ' - ' + CC.Code, 'N/A') AS ColorSortingCode
  , COALESCE(NULLIF(IT.[IsfWidth Code], ''), 'N/A') AS WidthCode
  , COALESCE(NULLIF(WC.Description, ''), 'N/A') AS WidthDescription
  , COALESCE(NULLIF(IT.[IsfWidth Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(WC.Description, ''), 'N/A') AS WidthCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(WC.Sorting, 0)) + ' - ' + WC.Code, 'N/A') AS WidthSortingCode
  , COALESCE(NULLIF(IT.[IsfLength Code], ''), 'N/A') AS LengthCode
  , COALESCE(NULLIF(LC.Description, ''), 'N/A') AS LengthDescription
  , COALESCE(NULLIF(IT.[IsfLength Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(LC.Description, ''), 'N/A') AS LengthCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(LC.Sorting, 0)) + ' - ' + LC.Code, 'N/A') AS LengthSortingCode
  , COALESCE(NULLIF(IT.[IsfQuality Code], ''), 'N/A') AS QualityCode
  , COALESCE(NULLIF(QC.Description, ''), 'N/A') AS QualityDescription
  , COALESCE(NULLIF(IT.[IsfQuality Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(QC.Description, ''), 'N/A') AS QualityCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(QC.Sorting, 0)) + ' - ' + QC.Code, 'N/A') AS QualitySortingCode
  , COALESCE(NULLIF(IT.[IsfCup Code], ''), 'N/A') AS CupCode
  , COALESCE(NULLIF(CUPC.Description, ''), 'N/A') AS CupDescription
  , COALESCE(NULLIF(IT.[IsfCup Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CUPC.Description, ''), 'N/A') AS CupCodeDescription
  , COALESCE(CONVERT(NVARCHAR(20), COALESCE(CUPC.Sorting, 0)) + ' - ' + CUPC.Code, 'N/A') AS CupSortingCode
  , COALESCE(NULLIF(IT.[IsfStyle No_], ''), 'N/A') AS StyleCode
  , COALESCE(NULLIF(STY.Description, ''), 'N/A') AS StyleDescription
  , COALESCE(NULLIF(IT.[IsfStyle No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STY.Description, ''), 'N/A') AS StyleCodeDescription
/* ISF */
FROM stage_nav.[Item] AS IT
LEFT JOIN stage_nav.[Unit Of Measure] AS UOM
  ON UOM.Code = IT.[Base Unit of Measure]
    AND UOM.company_id = IT.company_id
    AND UOM.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Inventory Posting Group] AS IPG
  ON IPG.Code = IT.[Inventory Posting Group]
    AND IPG.company_id = IT.company_id
    AND IPG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Item Discount Group] AS IDG
  ON IDG.Code = IT.[Item Disc_ Group]
    AND IDG.company_id = IT.company_id
    AND IDG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.Vendor AS V
  ON V.No_ = IT.[Vendor No_]
    AND V.company_id = IT.company_id
    AND V.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Gen_ Product Posting Group] AS GPPG
  ON GPPG.Code = IT.[Gen_ Prod_ Posting Group]
    AND GPPG.company_id = IT.company_id
    AND GPPG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Item Category] AS IC
  ON IC.[Code] = IT.[Item Category Code]
    AND IC.company_id = IT.company_id
    AND IC.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Product Group] AS PG
  ON PG.[Code] = IT.[Product Group Code]
    AND PG.[Item Category Code] = IT.[Item Category Code]
    AND PG.company_id = IT.company_id
    AND PG.data_connection_id = IT.data_connection_id
/* ISF */
LEFT JOIN stage_nav.[IsfAttribute] AS COLC
  ON COLC.Code = IT.[IsfCollection Code]
    AND COLC.company_id = IT.company_id
    AND COLC.data_connection_id = IT.data_connection_id
    AND COLC.[Type] = 0 --Collection
LEFT JOIN stage_nav.[IsfAttribute] AS SC
  ON SC.Code = IT.[IsfSeason Code]
    AND SC.company_id = IT.company_id
    AND SC.data_connection_id = IT.data_connection_id
    AND SC.[Type] = 1 --Season
LEFT JOIN stage_nav.[IsfAttribute] AS BC
  ON BC.Code = IT.[IsfBrand Code]
    AND BC.company_id = IT.company_id
    AND BC.data_connection_id = IT.data_connection_id
    AND BC.[Type] = 2 --Brand
LEFT JOIN stage_nav.[IsfAttribute] AS CC
  ON CC.Code = IT.[IsfColor Code]
    AND CC.company_id = IT.company_id
    AND CC.data_connection_id = IT.data_connection_id
    AND CC.[Type] = 3 --Color
LEFT JOIN stage_nav.[IsfAttribute] AS WC
  ON WC.Code = IT.[IsfWidth Code]
    AND WC.company_id = IT.company_id
    AND WC.data_connection_id = IT.data_connection_id
    AND WC.[Type] = 4 --Width
LEFT JOIN stage_nav.[IsfAttribute] AS LC
  ON LC.Code = IT.[IsfLength Code]
    AND LC.company_id = IT.company_id
    AND LC.data_connection_id = IT.data_connection_id
    AND LC.[Type] = 7 --Length
LEFT JOIN stage_nav.[IsfAttribute] AS QC
  ON QC.Code = IT.[IsfQuality Code]
    AND QC.company_id = IT.company_id
    AND QC.data_connection_id = IT.data_connection_id
    AND QC.[Type] = 8 --Quality
LEFT JOIN stage_nav.[IsfAttribute] AS CUPC
  ON CUPC.Code = IT.[IsfCup Code]
    AND CUPC.company_id = IT.company_id
    AND CUPC.data_connection_id = IT.data_connection_id
    AND CUPC.[Type] = 9 --Cup
LEFT JOIN stage_nav.Isfstyle AS STY
  ON STY.[No_] = IT.[IsfStyle No_]
    AND STY.Company_Id = IT.Company_Id
    AND STY.Data_Connection_Id = IT.Data_Connection_Id
    /* ISF */
