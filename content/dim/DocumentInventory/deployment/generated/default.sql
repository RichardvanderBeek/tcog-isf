SELECT
  -- System Information
  ILE.company_id AS CompanyID
  , ILE.data_connection_id AS DataConnectionID
  -- Business Key                                                                                       
  , ILE.[Document No_] AS DocumentNo
  , ILE.[Document Type] AS DocumentTypeCode
  -- attributes 
  , COALESCE(NULLIF(e_dt.OptionValue, ''), 'N/A') AS DocumentTypeDescription
  , 0 AS DocumentInventoryKeyType
  , 'Item Ledger Entry' AS DocumentInventoryKeyTypeDesc
FROM stage_nav.[Item Ledger Entry] AS ILE
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = ILE.[Document Type]
    AND e_dt.TableName = 'Item Ledger Entry'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.DataConnectionId = ILE.data_connection_id
GROUP BY ILE.company_id
  , ILE.data_connection_id
  , ILE.[Document No_]
  , ILE.[Document Type]
  , COALESCE(NULLIF(e_dt.OptionValue, ''), 'N/A')
