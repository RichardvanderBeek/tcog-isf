SELECT
  -- system information
  OOR.stage_id AS StageID
  , OOR.company_id AS CompanyID
  , OOR.data_connection_id AS DataConnectionID
  -- business key
  , OOR.Code AS OrderOriginCode
  --  attributes                        
  , COALESCE(NULLIF(OOR.Description, ''), 'N/A') AS OrderOriginDescription
  , COALESCE(NULLIF(OOR.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(OOR.Description, ''), 'N/A') AS OrderOriginCodeDescription
FROM stage_nav.[IsfOrder Origin] AS OOR
