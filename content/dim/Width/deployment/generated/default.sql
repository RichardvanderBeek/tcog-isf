SELECT
  -- system information
  WID.stage_id AS StageID
  , WID.company_id AS CompanyID
  , WID.data_connection_id AS DataConnectionID
  -- business key
  , WID.Code AS WidthCode
  --  attributes                        
  , COALESCE(NULLIF(WID.Description, ''), 'N/A') AS WidthDescription
  , COALESCE(NULLIF(WID.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(WID.Description, ''), 'N/A') AS WidthCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(WID.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), WID.Company_Id) + ' - ' + WID.Code AS WidthSortingCode
FROM stage_nav.[IsfAttribute] AS WID
WHERE WID.Type = 4 -- Width
