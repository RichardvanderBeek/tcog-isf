SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS CashflowSourceTypeCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS CashflowSourceTypeDesc
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'Cash Flow Forecast Entry'
  AND E_T.ColumnName = 'Source Type'
