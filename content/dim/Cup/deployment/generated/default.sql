SELECT
  -- system information
  CUP.stage_id AS StageID
  , CUP.company_id AS CompanyID
  , CUP.data_connection_id AS DataConnectionID
  -- business key
  , CUP.Code AS CupCode
  --  attributes                        
  , COALESCE(NULLIF(CUP.Description, ''), 'N/A') AS CupDescription
  , COALESCE(NULLIF(CUP.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(CUP.Description, ''), 'N/A') AS CupCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(CUP.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), CUP.Company_Id) + ' - ' + CUP.Code AS CupSortingCode
FROM stage_nav.[IsfAttribute] AS CUP
WHERE CUP.Type = 9 -- Cup
