SELECT
  -- system information
  SEA.stage_id AS StageID
  , SEA.company_id AS CompanyID
  , SEA.data_connection_id AS DataConnectionID
  -- business key
  , SEA.Code AS SeasonCode
  --  attributes                        
  , COALESCE(NULLIF(SEA.Description, ''), 'N/A') AS SeasonDescription
  , COALESCE(NULLIF(SEA.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SEA.Description, ''), 'N/A') AS SeasonCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(SEA.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), SEA.Company_Id) + ' - ' + SEA.Code AS SeasonSortingCode
FROM stage_nav.[IsfAttribute] AS SEA
WHERE SEA.Type = 1 -- Season
