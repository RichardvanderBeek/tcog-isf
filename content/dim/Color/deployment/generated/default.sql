SELECT
  -- system information
  COL.stage_id AS StageID
  , COL.company_id AS CompanyID
  , COL.data_connection_id AS DataConnectionID
  -- business key
  , COL.Code AS ColorCode
  --  attributes                        
  , COALESCE(NULLIF(COL.Description, ''), 'N/A') AS ColorDescription
  , COALESCE(NULLIF(COL.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(COL.Description, ''), 'N/A') AS ColorCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(COL.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), COL.Company_Id) + ' - ' + COL.Code AS ColorSortingCode
FROM stage_nav.[IsfAttribute] AS COL
WHERE COL.Type = 3 --Color
