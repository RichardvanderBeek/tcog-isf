SELECT
  -- system information
  CL.stage_id AS StageID
  , CL.company_id AS CompanyID
  , CL.data_connection_id AS DataConnectionID
  -- business key
  , CL.Code AS CollectionCode
  --  attributes                        
  , COALESCE(NULLIF(CL.Description, ''), 'N/A') AS CollectionDescription
  , COALESCE(NULLIF(CL.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(CL.Description, ''), 'N/A') AS CollectionCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(CL.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), CL.Company_Id) + ' - ' + CL.Code AS CollectionSortingCode
FROM stage_nav.[IsfAttribute] AS CL
WHERE CL.Type = 0 -- Collection
