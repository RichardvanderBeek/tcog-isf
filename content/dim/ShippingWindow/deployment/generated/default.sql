SELECT
  -- system information
  ISW.stage_id AS StageID
  , ISW.company_id AS CompanyID
  , ISW.data_connection_id AS DataConnectionID
  -- business key
  , ISW.Code AS ShippingWindowCode
  --  attributes                        
  , COALESCE(NULLIF(ISW.Description, ''), 'N/A') AS ShippingWindowDescription
  , COALESCE(NULLIF(ISW.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(ISW.Description, ''), 'N/A') AS ShippingWindowCodeDescription
  , COALESCE(NULLIF(ISW.[From Date], '1753-01-01 00:00:00.000'), '1900-01-01 00:00:00.000') AS ShippingFromDate
  , COALESCE(NULLIF(ISW.[Till Date], '1753-01-01 00:00:00.000'), '9999-12-31 00:00:00.000') AS ShippingTillDate
  , COALESCE(NULLIF(ISW.[Default Shipment Date], '1753-01-01 00:00:00.000'), '1900-01-01 00:00:00.000') AS ShippingDefaultDate
FROM stage_nav.[IsfShipping Window] AS ISW
