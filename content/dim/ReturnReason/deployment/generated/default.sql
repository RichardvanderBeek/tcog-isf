SELECT
  -- system information
  RR.stage_id AS StageID
  , RR.company_id AS CompanyID
  , RR.data_connection_id AS DataConnectionID
  -- business key
  , RR.Code AS ReturnReasonCode
  --  attributes                        
  , COALESCE(NULLIF(RR.Description, ''), 'N/A') AS ReturnReasonDescription
  , COALESCE(NULLIF(RR.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(RR.Description, ''), 'N/A') AS ReturnReasonCodeDescription
FROM stage_nav.[Return Reason] AS RR
