SELECT
  -- system information
  LE.stage_id AS StageID
  , LE.company_id AS CompanyID
  , LE.data_connection_id AS DataConnectionID
  -- business key
  , LE.Code AS LengthCode
  --  attributes                        
  , COALESCE(NULLIF(LE.Description, ''), 'N/A') AS LengthDescription
  , COALESCE(NULLIF(LE.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(LE.Description, ''), 'N/A') AS LengthCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(LE.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), LE.Company_Id) + ' - ' + LE.Code AS LengthSortingCode
FROM stage_nav.[IsfAttribute] AS LE
WHERE LE.Type = 7 -- Length
