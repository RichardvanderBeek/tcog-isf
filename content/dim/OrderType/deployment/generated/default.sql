SELECT
  -- system information
  e_OT.DataConnectionID AS DataConnectionID
  -- business key
  , e_OT.[OptionNo] AS OrderTypeCode
  --  attributes                        
  , COALESCE(NULLIF(e_OT.OptionValue, ''), 'N/A') AS OrderTypeDescription
  , CAST(COALESCE(e_OT.[OptionNo], 0) AS NVARCHAR(255)) + ' - ' + COALESCE(NULLIF(e_OT.OptionValue, ''), 'N/A') AS OrderTypeCodeDescription
FROM help.Enumerations AS e_OT
WHERE e_OT.ColumnName = 'IsfOrder Type'
  AND e_OT.TableName = 'Value Entry'
