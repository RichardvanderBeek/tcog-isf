WITH LastApplicationDate_CTE
AS (
  SELECT company_id AS CompanyID
    , data_connection_id AS DataConnectionID
    , [Cust_ Ledger Entry No_] AS CustLedgerEntryNo
    , MAX([Posting Date]) AS LastApplicationDate
  FROM stage_nav.[Detailed Cust_ Ledg_ Entry]
  WHERE [Entry Type] = 2 --Application
  GROUP BY company_id
    , data_connection_id
    , [Cust_ Ledger Entry No_]
  )
SELECT
  -- System Information
  CLE.company_id AS CompanyID
  , CLE.data_connection_id AS DataConnectionID
  -- Busineskey
  , CLE.[Entry No_] AS CustomerInvoiceCode
  -- Attributes
  , COALESCE(NULLIF(CLE.[Document No_], ''), 'N/A') AS CustomerInvoiceDocumentCode
  , COALESCE(CLE.[Document Type], - 1) AS CustomerInvoiceDocumentType
  , COALESCE(NULLIF(CLE.[External Document No_], ''), 'N/A') AS CustomerInvoiceExternalDocumentCode
  , COALESCE(e_dt.OptionValue, 'N/A') AS CustomerInvoiceDocumentTypeDescription
  , CLE.[Open] AS CustomerInvoiceOpenCode
  , COALESCE(e_o.OptionValue, 'N/A') AS CustomerInvoiceOpenDescription
  , CLE.[On Hold] AS CustomerInvoiceOnHold
  , COALESCE(NULLIF(CLE.[Posting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS CustomerInvoicePostingDate
  , COALESCE(NULLIF(CLE.[Due Date], '1753-01-01 00:00:00'), CLE.[Posting Date]) AS CustomerInvoiceDueDate
  , CASE 
    WHEN CLE.[Open] = 1
      THEN '9999-12-31 00:00:000'
    ELSE COALESCE(NULLIF(CLE.[Closed at Date], '1753-01-01 00:00:00'), NULLIF(LAD.[LastApplicationDate], '1753-01-01 00:00:00'), '9999-12-31 00:00:000')
    END AS CustomerInvoicePaidDate
FROM stage_nav.[Cust_ Ledger Entry] AS CLE
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = CLE.[Document Type]
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.TableName = 'Cust. Ledger Entry'
    AND e_dt.DataConnectionId = CLE.data_connection_id
LEFT JOIN help.Enumerations AS e_o
  ON e_o.OptionNo = CLE.[Open]
    AND e_o.ColumnName = 'Open'
    AND e_o.TableName = 'Cust. Ledger Entry'
    AND e_o.DataConnectionId = CLE.data_connection_id
LEFT JOIN LastApplicationDate_CTE AS LAD
  ON CLE.[Entry No_] = LAD.CustLedgerEntryNo
    AND CLE.company_id = LAD.CompanyID
    AND CLE.data_connection_id = LAD.DataConnectionID
