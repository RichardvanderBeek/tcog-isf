SELECT
  -- system information
  IPG.stage_id AS StageID
  , IPG.company_id AS CompanyID
  , IPG.data_connection_id AS DataConnectionID
  -- business key
  , IPG.Code AS InventoryPostingGroupCode
  --  attributes                        
  , COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS InventoryPostingGroupDescription
  , COALESCE(NULLIF(IPG.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS InventoryPostingGroupCodeDescription
FROM stage_nav.[Inventory Posting Group] AS IPG
