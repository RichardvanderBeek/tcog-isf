SELECT
  -- System Information
  CPV.stage_id AS StageID
  , CPV.company_id AS CompanyID
  , CPV.data_connection_id AS DataConnectionID
  -- Business Key
  , CPV.Code AS CustomerPostingGroupCode
  -- Attributes
  , CPV.Code AS CustomerPostingGroupDesc
FROM stage_nav.[Customer Posting Group] AS CPV
