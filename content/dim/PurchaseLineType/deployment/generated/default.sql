SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS PurchaseLineTypeCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS PurchaseLineTypeDescription
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'Purch. Inv. Line'
  AND E_T.ColumnName = 'Type'
