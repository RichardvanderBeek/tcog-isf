SELECT
  -- system information
  STY.stage_id AS StageID
  , STY.company_id AS CompanyID
  , STY.data_connection_id AS DataConnectionID
  -- business key
  , STY.[No_] AS StyleCode
  --  attributes                        
  , COALESCE(NULLIF(STY.Description, ''), 'N/A') AS StyleDescription
  , COALESCE(NULLIF(STY.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STY.Description, ''), 'N/A') AS StyleCodeDescription
FROM stage_nav.[IsfStyle] AS STY
