SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS ActivityTypeCode
  --  Attributes
  , E_T.OptionValue AS ActivityTypeDescription
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'Warehouse Activity Line'
  AND E_T.ColumnName = 'Activity Type'
