SELECT
  -- system information
  IBN.stage_id AS StageID
  , IBN.company_id AS CompanyID
  , IBN.data_connection_id AS DataConnectionID
  -- business key
  , IBN.NAME AS SalesBudgetCode
  --  attributes                        
  , COALESCE(NULLIF(IBN.Description, ''), 'N/A') AS SalesBudgetDescription
  , COALESCE(NULLIF(IBN.NAME, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IBN.Description, ''), 'N/A') AS SalesBudgetCodeDescription
FROM stage_nav.[Item Budget Name] AS IBN
WHERE IBN.[Analysis Area] = 2;-- Sales
