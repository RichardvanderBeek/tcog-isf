SELECT
  -- system information
  BR.stage_id AS StageID
  , BR.company_id AS CompanyID
  , BR.data_connection_id AS DataConnectionID
  -- business key
  , BR.Code AS BrandCode
  --  attributes                        
  , COALESCE(NULLIF(BR.Description, ''), 'N/A') AS BrandDescription
  , COALESCE(NULLIF(BR.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(BR.Description, ''), 'N/A') AS BrandCodeDescription
  , CONVERT(NVARCHAR(20), COALESCE(BR.Sorting, 0)) + ' - ' + CONVERT(NVARCHAR(100), BR.Company_Id) + ' - ' + BR.Code AS BrandSortingCode
FROM stage_nav.[IsfAttribute] AS BR
WHERE BR.Type = 2;-- Brand
