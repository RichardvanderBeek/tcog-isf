SELECT
  -- system information
  LC.stage_id AS StageID
  , LC.company_id AS CompanyID
  , LC.data_connection_id AS DataConnectionID
  -- business key
  , LC.[Code] AS LocationCode
  --  attributes                        
  , COALESCE(NULLIF(LC.NAME, ''), 'N/A') AS LocationDescription
  , COALESCE(NULLIF(LC.[Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(LC.NAME, ''), 'N/A') AS LocationCodeDescription
  , COALESCE(NULLIF(LC.[Country_Region Code], ''), 'N/A') AS LocationCountryCode
  , COALESCE(NULLIF(CR.[Name], ''), 'N/A') AS LocationCountryName
  , COALESCE(NULLIF(LC.[Country_Region Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CR.[Name], ''), 'N/A') AS LocationCountryCodeName
  , COALESCE(NULLIF(LC.[Post Code], ''), 'N/A') AS LocationPostCode
  , COALESCE(NULLIF(LC.Address, ''), 'N/A') AS LocationAddress
  , COALESCE(NULLIF(LC.City, ''), 'N/A') AS LocationCity
FROM stage_nav.[Location] AS LC
LEFT JOIN stage_nav.[Country_Region] AS CR
  ON CR.Code = LC.[Country_Region Code]
    AND CR.Company_Id = LC.Company_Id
    AND CR.Data_Connection_Id = LC.Data_Connection_Id
