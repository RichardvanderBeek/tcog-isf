WITH LastApplicationDate_CTE
AS (
  SELECT company_id AS CompanyID
    , data_connection_id AS DataConnectionID
    , [Vendor Ledger Entry No_] AS VendorLedgerEntryNo
    , MAX([Posting Date]) AS LastApplicationDate
  FROM stage_nav.[Detailed Vendor Ledg_ Entry]
  WHERE [Entry Type] = 2 --Application
  GROUP BY company_id
    , data_connection_id
    , [Vendor Ledger Entry No_]
  )
SELECT
  -- System Information
  VLE.company_id AS CompanyID
  , VLE.data_connection_id AS DataConnectionID
  -- Busineskey
  , VLE.[Entry No_] AS VendorInvoiceCode
  -- Attributes
  , COALESCE(NULLIF(VLE.[Document No_], ''), 'N/A') AS VendorInvoiceDocumentCode
  , COALESCE(VLE.[Document Type], - 1) AS VendorInvoiceDocumentType
  , COALESCE(NULLIF(VLE.[External Document No_], ''), 'N/A') AS VendorInvoiceExternalDocumentCode
  , COALESCE(e_dt.OptionValue, 'N/A') AS VendorInvoiceDocumentTypeDescription
  , VLE.[Open] AS VendorInvoiceOpenCode
  , COALESCE(e_o.OptionValue, 'N/A') AS VendorInvoiceOpenDescription
  , VLE.[On Hold] AS VendorInvoiceOnHold
  , COALESCE(NULLIF(VLE.[Posting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS VendorInvoicePostingDate
  , COALESCE(NULLIF(VLE.[Due Date], '1753-01-01 00:00:00'), VLE.[Posting Date]) AS VendorInvoiceDueDate
  , CASE 
    WHEN VLE.[Open] = 1
      THEN '9999-12-31 00:00:000'
    ELSE COALESCE(NULLIF(VLE.[Closed at Date], '1753-01-01 00:00:00'), NULLIF(LAD.[LastApplicationDate], '1753-01-01 00:00:00'), '9999-12-31 00:00:000')
    END AS VendorInvoicePaidDate
FROM stage_nav.[Vendor Ledger Entry] AS VLE
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = VLE.[Document Type]
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.TableName = 'Vendor Ledger Entry'
    AND e_dt.DataConnectionId = VLE.data_connection_id
LEFT JOIN help.Enumerations AS e_o
  ON e_o.OptionNo = VLE.[Open]
    AND e_o.ColumnName = 'Open'
    AND e_o.TableName = 'Vendor Ledger Entry'
    AND e_o.DataConnectionId = VLE.data_connection_id
LEFT JOIN LastApplicationDate_CTE AS LAD
  ON VLE.[Entry No_] = LAD.VendorLedgerEntryNo
    AND VLE.company_id = LAD.CompanyID
    AND VLE.data_connection_id = LAD.DataConnectionID
