/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseCreditMemoLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseCreditMemoLineView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseCreditMemoLineView
AS
  SELECT
    -- system information
                    PCML.stage_id                                      AS StageID
                  , PCML.company_id                                    AS CompanyID
                  , PCML.data_connection_id                            AS DataConnectionID
                  -- business key
                  , VE.[Entry No_]                                     AS EntryNo
                  , VE.[Document Type]                                 AS DocumentType
                  , VE.[Document No_]                                  AS DocumentNo
                  , VE.[Posting Date]                                  AS PostingDate
                  , COALESCE(PCMH.[Posting Date], PCML.[Posting Date]) AS DocumentPostingDate
                  , PCML.[Buy-from Vendor No_]                         AS BuyFromVendorNo
                  , PCML.[Pay-to Vendor No_]                           AS PayToVendorNo
                  , COALESCE(PCMH.[Currency Factor], 1)                AS CurrencyFactor
                  , PCMH.[Currency Code]                               AS CurrencyCode
                  , PCML.[Type]                                        AS [Type]
                  /* ISF */
                  , VE.[IsfBrand Code]                                 AS BrandCode
                  , VE.[IsfCollection Code]                            AS CollectionCode
                  , VE.[IsfColor Code]                                 AS ColorCode
                  , VE.[IsfSeason Code]                                AS SeasonCode
                  , VE.[IsfStyle No_]                                  AS StyleCode
                  , VE.[IsfOrder Type]                                 AS OrderTypeCode
                  , VE.[IsfCup Code]                                   AS CupCode
                  , VE.[IsfWidth Code]                                 AS WidthCode
                  , VE.[IsfLength]                                     AS LengthCode
                  , VE.[IsfOrder Origin]                               AS OrderOriginCode
                  , VE.[IsfQuality Code]                               AS QualityCode
    /* ISF */
    FROM            stage_nav.[Purch_ Cr_ Memo Line] AS PCML
    LEFT OUTER JOIN stage_nav.[Purch_ Cr_ Memo Hdr_] AS PCMH ON PCML.[Document No_]       = PCMH.[No_]
                                                            AND PCML.company_id           = PCMH.company_id
                                                            AND PCML.data_connection_id   = PCMH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]          AS VE ON VE.[Document Type]          = 8 --Credit Memo
                                                          AND PCML.[Document No_]         = VE.[Document No_]
                                                          AND PCML.[Line No_]             = VE.[Document Line No_]
                                                          AND VE.[Item Ledger Entry Type] = 0 --Purchase
                                                          AND PCML.company_id             = VE.company_id
                                                          AND PCML.data_connection_id     = VE.data_connection_id
   WHERE            PCML.execution_flag <> 'D'
     AND            PCMH.execution_flag            <> 'D'
     AND            VE.execution_flag              <> 'D' ;
GO
