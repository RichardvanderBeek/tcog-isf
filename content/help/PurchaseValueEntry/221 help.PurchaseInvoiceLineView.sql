/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseInvoiceLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseInvoiceLineView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseInvoiceLineView
AS
  SELECT
    -- system information
                    PIL.stage_id                                     AS StageID
                  , PIL.company_id                                   AS CompanyID
                  , PIL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , VE.[Entry No_]                                   AS EntryNo
                  , VE.[Document Type]                               AS DocumentType
                  , VE.[Document No_]                                AS DocumentNo
                  , VE.[Posting Date]                                AS PostingDate
                  , COALESCE(PIH.[Posting Date], PIL.[Posting Date]) AS DocumentPostingDate
                  , PIL.[Buy-from Vendor No_]                        AS BuyFromVendorNo
                  , PIL.[Pay-to Vendor No_]                          AS PayToVendorNo
                  , COALESCE(PIH.[Currency Factor], 1)               AS CurrencyFactor
                  , PIH.[Currency Code]                              AS CurrencyCode
                  , PIL.[Type]                                       AS [Type]
                  /* ISF */
                  , VE.[IsfBrand Code]                               AS BrandCode
                  , VE.[IsfCollection Code]                          AS CollectionCode
                  , VE.[IsfColor Code]                               AS ColorCode
                  , VE.[IsfSeason Code]                              AS SeasonCode
                  , VE.[IsfStyle No_]                                AS StyleCode
                  , VE.[IsfOrder Type]                               AS OrderTypeCode
                  , VE.[IsfCup Code]                                 AS CupCode
                  , VE.[IsfWidth Code]                               AS WidthCode
                  , VE.[IsfLength]                                   AS LengthCode
                  , VE.[IsfOrder Origin]                             AS OrderOriginCode
                  , VE.[IsfQuality Code]                             AS QualityCode
    /* ISF */
    FROM            stage_nav.[Purch_ Inv_ Line]   AS PIL
    LEFT OUTER JOIN stage_nav.[Purch_ Inv_ Header] AS PIH ON PIL.[Document No_]         = PIH.[No_]
                                                         AND PIL.company_id             = PIH.company_id
                                                         AND PIL.data_connection_id     = PIH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]        AS VE ON VE.[Document Type]          = 6 --Invoices
                                                        AND PIL.[Document No_]          = VE.[Document No_]
                                                        AND PIL.[Line No_]              = VE.[Document Line No_]
                                                        AND VE.[Item Ledger Entry Type] = 0 --Purchase
                                                        AND PIL.company_id              = VE.company_id
                                                        AND PIL.data_connection_id      = VE.data_connection_id
   WHERE            PIL.execution_flag <> 'D'
     AND            PIH.execution_flag            <> 'D'
     AND            VE.execution_flag             <> 'D' ;
GO
