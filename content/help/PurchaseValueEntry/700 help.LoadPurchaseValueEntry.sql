/***********************************************************************************
Functionality:  This SQL script creates the help.LoadPurchaseValueEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadPurchaseValueEntry', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadPurchaseValueEntry
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  -- full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM help.PurchaseValueEntry ;
    INSERT INTO help.PurchaseValueEntry (
      --system information
      StageID
    , Company_ID
    , Data_Connection_ID
    , Component_Execution_ID
    , Execution_Flag
    , EntryNo
    , DocumentType
    , DocumentNo
    , PostingDate
    , DocumentPostingDate
    , BuyFromVendorNo
    , PayToVendorNo
    , CurrencyFactor
    , CurrencyCode
    , [Type]
    /* ISF */
    , BrandCode
    , CollectionCode
    , ColorCode
    , SeasonCode
    , StyleCode
    , OrderTypeCode
    , CupCode
    , WidthCode
    , LengthCode
    , OrderOriginCode
    , QualityCode
    /* ISF */
    )
    SELECT
      --system information
           PVEUV.StageID
         , PVEUV.CompanyID
         , PVEUV.DataConnectionID
         , @component_execution_id
         , @execution_flag
         , PVEUV.EntryNo
         , PVEUV.DocumentType
         , PVEUV.DocumentNo
         , PVEUV.PostingDate
         , PVEUV.DocumentPostingDate
         , PVEUV.BuyFromVendorNo
         , PVEUV.PayToVendorNo
         , PVEUV.CurrencyFactor
         , PVEUV.CurrencyCode
         , PVEUV.[Type]
         /* ISF */
         , PVEUV.BrandCode
         , PVEUV.CollectionCode
         , PVEUV.ColorCode
         , PVEUV.SeasonCode
         , PVEUV.StyleCode
         , PVEUV.OrderTypeCode
         , PVEUV.CupCode
         , PVEUV.WidthCode
         , PVEUV.LengthCode
         , PVEUV.OrderOriginCode
         , PVEUV.QualityCode
      /* ISF */
      FROM help.PurchaseValueEntryUnionView AS PVEUV ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;