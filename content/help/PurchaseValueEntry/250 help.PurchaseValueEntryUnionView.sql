/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseValueEntryUnionView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseValueEntryUnionView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseValueEntryUnionView
AS
  SELECT PRLV.StageID
       , PRLV.CompanyID
       , PRLV.DataConnectionID
       , PRLV.EntryNo
       , PRLV.DocumentType
       , PRLV.DocumentNo
       , PRLV.PostingDate
       , PRLV.DocumentPostingDate
       , PRLV.BuyFromVendorNo
       , PRLV.PayToVendorNo
       , PRLV.CurrencyFactor
       , PRLV.CurrencyCode
       , PRLV.[Type]
       /* ISF */
       , PRLV.BrandCode
       , PRLV.CollectionCode
       , PRLV.ColorCode
       , PRLV.SeasonCode
       , PRLV.StyleCode
       , PRLV.OrderTypeCode
       , PRLV.CupCode
       , PRLV.WidthCode
       , PRLV.LengthCode
       , PRLV.OrderOriginCode
       , PRLV.QualityCode
    /* ISF */
    FROM help.PurchaseReceiptLineView AS PRLV
  UNION ALL
  SELECT PILV.StageID
       , PILV.CompanyID
       , PILV.DataConnectionID
       , PILV.EntryNo
       , PILV.DocumentType
       , PILV.DocumentNo
       , PILV.PostingDate
       , PILV.DocumentPostingDate
       , PILV.BuyFromVendorNo
       , PILV.PayToVendorNo
       , PILV.CurrencyFactor
       , PILV.CurrencyCode
       , PILV.[Type]
       /* ISF */
       , PILV.BrandCode
       , PILV.CollectionCode
       , PILV.ColorCode
       , PILV.SeasonCode
       , PILV.StyleCode
       , PILV.OrderTypeCode
       , PILV.CupCode
       , PILV.WidthCode
       , PILV.LengthCode
       , PILV.OrderOriginCode
       , PILV.QualityCode
    /* ISF */
    FROM help.PurchaseInvoiceLineView AS PILV
  UNION ALL
  SELECT RSLV.StageID
       , RSLV.CompanyID
       , RSLV.DataConnectionID
       , RSLV.EntryNo
       , RSLV.DocumentType
       , RSLV.DocumentNo
       , RSLV.PostingDate
       , RSLV.DocumentPostingDate
       , RSLV.BuyFromVendorNo
       , RSLV.PayToVendorNo
       , RSLV.CurrencyFactor
       , RSLV.CurrencyCode
       , RSLV.[Type]
       /* ISF */
       , RSLV.BrandCode
       , RSLV.CollectionCode
       , RSLV.ColorCode
       , RSLV.SeasonCode
       , RSLV.StyleCode
       , RSLV.OrderTypeCode
       , RSLV.CupCode
       , RSLV.WidthCode
       , RSLV.LengthCode
       , RSLV.OrderOriginCode
       , RSLV.QualityCode
    /* ISF */
    FROM help.ReturnShipmentLineView AS RSLV
  UNION ALL
  SELECT PCMLV.StageID
       , PCMLV.CompanyID
       , PCMLV.DataConnectionID
       , PCMLV.EntryNo
       , PCMLV.DocumentType
       , PCMLV.DocumentNo
       , PCMLV.PostingDate
       , PCMLV.DocumentPostingDate
       , PCMLV.BuyFromVendorNo
       , PCMLV.PayToVendorNo
       , PCMLV.CurrencyFactor
       , PCMLV.CurrencyCode
       , PCMLV.[Type]
       /* ISF */
       , PCMLV.BrandCode
       , PCMLV.CollectionCode
       , PCMLV.ColorCode
       , PCMLV.SeasonCode
       , PCMLV.StyleCode
       , PCMLV.OrderTypeCode
       , PCMLV.CupCode
       , PCMLV.WidthCode
       , PCMLV.LengthCode
       , PCMLV.OrderOriginCode
       , PCMLV.QualityCode
    /* ISF */
    FROM help.PurchaseCreditMemoLineView AS PCMLV
  UNION ALL
  SELECT PVEV.StageID
       , PVEV.CompanyID
       , PVEV.DataConnectionID
       , PVEV.EntryNo
       , PVEV.DocumentType
       , PVEV.DocumentNo
       , PVEV.PostingDate
       , PVEV.DocumentPostingDate
       , PVEV.BuyFromVendorNo
       , PVEV.PayToVendorNo
       , PVEV.CurrencyFactor
       , PVEV.CurrencyCode
       , PVEV.[Type]
       /* ISF */
       , PVEV.BrandCode
       , PVEV.CollectionCode
       , PVEV.ColorCode
       , PVEV.SeasonCode
       , PVEV.StyleCode
       , PVEV.OrderTypeCode
       , PVEV.CupCode
       , PVEV.WidthCode
       , PVEV.LengthCode
       , PVEV.OrderOriginCode
       , PVEV.QualityCode
    /* ISF */
    FROM help.PurchaseValueEntryView AS PVEV ;
