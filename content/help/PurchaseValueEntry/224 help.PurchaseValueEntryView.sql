/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseValueEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseValueEntryView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseValueEntryView
AS
  SELECT
    -- system information
         VE.stage_id             AS StageID
       , VE.Company_Id           AS CompanyID
       , VE.data_connection_id   AS DataConnectionID
                                                 -- business key
       , VE.[Entry No_]          AS EntryNo
       , 0                       AS DocumentType --Empty Document Type
       , VE.[Document No_]       AS DocumentNo
       , VE.[Posting Date]       AS PostingDate
       , VE.[Posting Date]       AS DocumentPostingDate
       , ''                      AS BuyFromVendorNo
       , ''                      AS PayToVendorNo
       , 1                       AS CurrencyFactor
       , ''                      AS CurrencyCode
       , VE.[Type]               AS [Type]
                                                 /* ISF */
       , VE.[IsfBrand Code]      AS BrandCode
       , VE.[IsfCollection Code] AS CollectionCode
       , VE.[IsfColor Code]      AS ColorCode
       , VE.[IsfSeason Code]     AS SeasonCode
       , VE.[IsfStyle No_]       AS StyleCode
       , VE.[IsfOrder Type]      AS OrderTypeCode
       , VE.[IsfCup Code]        AS CupCode
       , VE.[IsfWidth Code]      AS WidthCode
       , VE.[IsfLength]          AS LengthCode
       , VE.[IsfOrder Origin]    AS OrderOriginCode
       , VE.[IsfQuality Code]    AS QualityCode
    /* ISF */
    FROM stage_nav.[Value Entry] AS VE
   WHERE VE.[Item Ledger Entry Type] = 0
     AND VE.[Document Type]          = 0
     AND VE.execution_flag           <> 'D' ;
GO
