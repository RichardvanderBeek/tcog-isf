/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseItemLedgerEntryUnionView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseItemLedgerEntryUnionView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseItemLedgerEntryUnionView
AS
  SELECT PRLV.StageID
       , PRLV.CompanyID
       , PRLV.DataConnectionID
       , PRLV.EntryNo
       , PRLV.DocumentType
       , PRLV.DocumentNo
       , PRLV.PostingDate
       , PRLV.DocumentPostingDate
       , PRLV.BuyFromVendorNo
       , PRLV.PayToVendorNo
       /* ISF */
       , PRLV.BrandCode
       , PRLV.CollectionCode
       , PRLV.ColorCode
       , PRLV.SeasonCode
       , PRLV.StyleCode
       , PRLV.OrderTypeCode
       , PRLV.CupCode
       , PRLV.WidthCode
       , PRLV.LengthCode
       , PRLV.OrderOriginCode
       , PRLV.QualityCode
    /* ISF */
    FROM help.PurchaseReceiptLineItemLedgerEntryView AS PRLV
  UNION ALL
  SELECT PILV.StageID
       , PILV.CompanyID
       , PILV.DataConnectionID
       , PILV.EntryNo
       , PILV.DocumentType
       , PILV.DocumentNo
       , PILV.PostingDate
       , PILV.DocumentPostingDate
       , PILV.BuyFromVendorNo
       , PILV.PayToVendorNo
       /* ISF */
       , PILV.BrandCode
       , PILV.CollectionCode
       , PILV.ColorCode
       , PILV.SeasonCode
       , PILV.StyleCode
       , PILV.OrderTypeCode
       , PILV.CupCode
       , PILV.WidthCode
       , PILV.LengthCode
       , PILV.OrderOriginCode
       , PILV.QualityCode
    /* ISF */
    FROM help.PurchaseInvoiceLineItemLedgerEntryView AS PILV
  UNION ALL
  SELECT RSLV.StageID
       , RSLV.CompanyID
       , RSLV.DataConnectionID
       , RSLV.EntryNo
       , RSLV.DocumentType
       , RSLV.DocumentNo
       , RSLV.PostingDate
       , RSLV.DocumentPostingDate
       , RSLV.BuyFromVendorNo
       , RSLV.PayToVendorNo
       /* ISF */
       , RSLV.BrandCode
       , RSLV.CollectionCode
       , RSLV.ColorCode
       , RSLV.SeasonCode
       , RSLV.StyleCode
       , RSLV.OrderTypeCode
       , RSLV.CupCode
       , RSLV.WidthCode
       , RSLV.LengthCode
       , RSLV.OrderOriginCode
       , RSLV.QualityCode
    /* ISF */
    FROM help.ReturnShipmentLineItemLedgerEntryView AS RSLV
  UNION ALL
  SELECT PCMLV.StageID
       , PCMLV.CompanyID
       , PCMLV.DataConnectionID
       , PCMLV.EntryNo
       , PCMLV.DocumentType
       , PCMLV.DocumentNo
       , PCMLV.PostingDate
       , PCMLV.DocumentPostingDate
       , PCMLV.BuyFromVendorNo
       , PCMLV.PayToVendorNo
       /* ISF */
       , PCMLV.BrandCode
       , PCMLV.CollectionCode
       , PCMLV.ColorCode
       , PCMLV.SeasonCode
       , PCMLV.StyleCode
       , PCMLV.OrderTypeCode
       , PCMLV.CupCode
       , PCMLV.WidthCode
       , PCMLV.LengthCode
       , PCMLV.OrderOriginCode
       , PCMLV.QualityCode
    /* ISF */
    FROM help.PurchaseCreditMemoLineItemLedgerEntryView AS PCMLV
  UNION ALL
  SELECT PVEV.StageID
       , PVEV.CompanyID
       , PVEV.DataConnectionID
       , PVEV.EntryNo
       , PVEV.DocumentType
       , PVEV.DocumentNo
       , PVEV.PostingDate
       , PVEV.DocumentPostingDate
       , PVEV.BuyFromVendorNo
       , PVEV.PayToVendorNo
       /* ISF */
       , PVEV.BrandCode
       , PVEV.CollectionCode
       , PVEV.ColorCode
       , PVEV.SeasonCode
       , PVEV.StyleCode
       , PVEV.OrderTypeCode
       , PVEV.CupCode
       , PVEV.WidthCode
       , PVEV.LengthCode
       , PVEV.OrderOriginCode
       , PVEV.QualityCode
    /* ISF */
    FROM help.PurchaseItemLedgerEntryView AS PVEV ;
