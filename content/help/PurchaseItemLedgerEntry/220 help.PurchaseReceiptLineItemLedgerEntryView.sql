/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseReceiptLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseReceiptLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseReceiptLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    PRL.stage_id                                     AS StageID
                  , PRL.company_id                                   AS CompanyID
                  , PRL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                  AS EntryNo
                  , ILE.[Document Type]                              AS DocumentType
                  , ILE.[Document No_]                               AS DocumentNo
                  , ILE.[Posting Date]                               AS PostingDate
                  , COALESCE(PRH.[Posting Date], PRL.[Posting Date]) AS DocumentPostingDate
                  , PRL.[Buy-from Vendor No_]                        AS BuyFromVendorNo
                  , PRL.[Pay-to Vendor No_]                          AS PayToVendorNo
                  /* ISF */
                  , ILE.[IsfBrand Code]                              AS BrandCode
                  , ILE.[IsfCollection Code]                         AS CollectionCode
                  , ILE.[IsfColor Code]                              AS ColorCode
                  , ILE.[IsfSeason Code]                             AS SeasonCode
                  , ILE.[IsfStyle No_]                               AS StyleCode
                  , ILE.[IsfOrder Type]                              AS OrderTypeCode
                  , ILE.[IsfCup Code]                                AS CupCode
                  , ILE.[IsfWidth Code]                              AS WidthCode
                  , ILE.[IsfLength]                                  AS LengthCode
                  , ILE.[IsfOrder Origin]                            AS OrderOriginCode
                  , ILE.[IsfQuality Code]                            AS QualityCode
    /* ISF */
    FROM            stage_nav.[Purch_ Rcpt_ Line]   AS PRL
    LEFT OUTER JOIN stage_nav.[Purch_ Rcpt_ Header] AS PRH ON PRL.[Document No_]     = PRH.[No_]
                                                          AND PRL.company_id         = PRH.company_id
                                                          AND PRL.data_connection_id = PRH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]   AS ILE ON ILE.[Document Type]    = 5 --Receipt
                                                          AND PRL.[Document No_]     = ILE.[Document No_]
                                                          AND PRL.[Line No_]         = ILE.[Document Line No_]
                                                          AND ILE.[Entry Type]       = 0 --Purchase  
                                                          AND PRL.company_id         = ILE.company_id
                                                          AND PRL.data_connection_id = ILE.data_connection_id
   WHERE            PRL.execution_flag <> 'D'
     AND            PRH.execution_flag            <> 'D'
     AND            ILE.execution_flag            <> 'D' ;
GO
