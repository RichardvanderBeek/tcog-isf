/***********************************************************************************
Functionality:  This SQL script creates the help.LoadPurchaseItemLedgerEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadPurchaseItemLedgerEntry', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadPurchaseItemLedgerEntry
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  -- full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM help.PurchaseItemLedgerEntry ;
    INSERT INTO help.PurchaseItemLedgerEntry (
      --system information
      StageID
    , Company_ID
    , Data_Connection_ID
    , Component_Execution_ID
    , Execution_Flag
    , EntryNo
    , DocumentType
    , DocumentNo
    , PostingDate
    , DocumentPostingDate
    , BuyFromVendorNo
    , PayToVendorNo
    /* ISF */
    , BrandCode
    , CollectionCode
    , ColorCode
    , SeasonCode
    , StyleCode
    , OrderTypeCode
    , CupCode
    , WidthCode
    , LengthCode
    , OrderOriginCode
    , QualityCode
    /* ISF */
    )
    SELECT
      --system information
           PILEV.StageID
         , PILEV.CompanyID
         , PILEV.DataConnectionID
         , @component_execution_id
         , @execution_flag
         , PILEV.EntryNo
         , PILEV.DocumentType
         , PILEV.DocumentNo
         , PILEV.PostingDate
         , PILEV.DocumentPostingDate
         , PILEV.BuyFromVendorNo
         , PILEV.PayToVendorNo
         /* ISF */
         , PILEV.BrandCode
         , PILEV.CollectionCode
         , PILEV.ColorCode
         , PILEV.SeasonCode
         , PILEV.StyleCode
         , PILEV.OrderTypeCode
         , PILEV.CupCode
         , PILEV.WidthCode
         , PILEV.LengthCode
         , PILEV.OrderOriginCode
         , PILEV.QualityCode
      /* ISF */
      FROM help.PurchaseItemLedgerEntryUnionView AS PILEV ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;