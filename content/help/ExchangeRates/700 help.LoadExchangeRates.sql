EXEC dbo.drop_object @object = N'help.LoadExchangeRates', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadExchangeRates
  @component_execution_id INT     = -1
, @load_type              TINYINT = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;

  IF @load_type <> 0 --incremental, not supported--> full load
  BEGIN
    --log warning
    DECLARE @proc sysname = dbo.get_proc_full_name(@@PROCID) ;
    EXEC [audit].[log_warning_unsupported_load_type] @procedure = @proc
                                                   , @load_type = @load_type
                                                   , @component_execution_id = @component_execution_id ;
    SELECT @deleted = COUNT(1) FROM help.ExchangeRates ;
    EXECUTE dbo.truncate_table 'help.ExchangeRates' ;
  END ;

  INSERT INTO help.ExchangeRates WITH (TABLOCK) (
    CrossRate
  , ToCurrencyCode
  , ValidFrom
  , ValidTo
  -- System
  , DataConnectionID
  , CompanyId
  , ComponentExecutionID
  )
  SELECT CrossRate
       , ToCurrencyCode
       , ValidFrom
       , ValidTo
       , DataConnectionId
       , CompanyId
       , COALESCE(@component_execution_id, -1)
    FROM help.ExchangeRatesView ;

  SELECT @inserted = @@ROWCOUNT ;

  INSERT INTO help.ExchangeRates WITH (TABLOCK) (
    CrossRate
  , ToCurrencyCode
  , ValidFrom
  , ValidTo
  -- System
  , DataConnectionID
  , CompanyId
  , ComponentExecutionID
  )
  SELECT CrossRate
       , ToCurrencyCode
       , ValidFrom
       , ValidTo
       , DataConnectionId
       , CompanyId
       , COALESCE(@component_execution_id, -1)
    FROM help.ExchangeRatesDefaultView ;

  SELECT @inserted += @@ROWCOUNT ;

  SELECT @updated = 0 ;

END ;