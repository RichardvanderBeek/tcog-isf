-- ################################################################################ --
-- #####                     Table: help.ExchangeRates	                          ##### --
-- ################################################################################ --
EXEC dbo.drop_object @object = N'help.ExchangeRates', @type = N'T' ;

CREATE TABLE help.ExchangeRates
(
  CrossRate            DECIMAL(38, 20) NOT NULL --only this rate is used in the BI
, ToCurrencyCode       NVARCHAR(20)    NOT NULL
, ValidFrom            DATETIME        NOT NULL
, ValidTo              DATETIME        NOT NULL
                                                -- System
, DataConnectionID     INT             NOT NULL
, CompanyId            INT             NOT NULL
, ExecutionTimestamp   ROWVERSION      NOT NULL
, ComponentExecutionID INT             NOT NULL
) ;
