EXEC dbo.drop_object @object = N'help.Enumerations', @type = N'T' ;
GO
CREATE TABLE help.Enumerations
(
  EnumerationsId       INT           IDENTITY(1, 1) NOT NULL
, ComponentExecutionId INT           NOT NULL
, DataConnectionId     INT           NOT NULL
, TableName            NVARCHAR(128) NOT NULL
, ColumnName           NVARCHAR(128) NOT NULL
, OptionNo             INT           NOT NULL
, OptionValue          NVARCHAR(255) NOT NULL
, IsCustom             BIT           NOT NULL DEFAULT (1)
) ;