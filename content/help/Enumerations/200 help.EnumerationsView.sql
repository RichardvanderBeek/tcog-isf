EXEC dbo.drop_object @object = N'help.EnumerationsView', @type = N'V' ;
GO
CREATE VIEW help.EnumerationsView
AS
  SELECT nseo.data_connection_id AS DataConnectionId
       -- bk
       , nseo.table_name         AS TableName
       , nseo.column_name        AS ColumnName
       , nseo.option_no          AS OptionNo
       -- attributes
       , nseo.option_value       AS OptionValue
    FROM meta.nav_source_enumeration_options AS nseo ;