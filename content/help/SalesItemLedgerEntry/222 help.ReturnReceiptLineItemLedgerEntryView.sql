/***********************************************************************************
Functionality:  This SQL script creates the help.ReturnReceiptLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.ReturnReceiptLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.ReturnReceiptLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    RRL.stage_id                                     AS StageID
                  , RRL.company_id                                   AS CompanyID
                  , RRL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                  AS EntryNo
                  , ILE.[Document Type]                              AS DocumentType
                  , ILE.[Document No_]                               AS DocumentNo
                  , ILE.[Posting Date]                               AS PostingDate
                  , COALESCE(RRH.[Posting Date], RRL.[Posting Date]) AS DocumentPostingDate
                  , RRL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , RRL.[Bill-to Customer No_]                       AS BilltoCustomerNo
                  /* ISF */
                  , ILE.[IsfBrand Code]                              AS BrandCode
                  , ILE.[IsfCollection Code]                         AS CollectionCode
                  , ILE.[IsfColor Code]                              AS ColorCode
                  , ILE.[IsfSeason Code]                             AS SeasonCode
                  , ILE.[IsfStyle No_]                               AS StyleCode
                  , ILE.[IsfOrder Type]                              AS OrderTypeCode
                  , ILE.[IsfCup Code]                                AS CupCode
                  , ILE.[IsfWidth Code]                              AS WidthCode
                  , ILE.[IsfLength]                                  AS LengthCode
                  , ILE.[IsfOrder Origin]                            AS OrderOriginCode
                  , ILE.[IsfQuality Code]                            AS QualityCode
    /* ISF */
    FROM            stage_nav.[Return Receipt Line]   AS RRL
    LEFT OUTER JOIN stage_nav.[Return Receipt Header] AS RRH ON RRL.[Document No_]     = RRH.[No_]
                                                            AND RRL.company_id         = RRH.company_id
                                                            AND RRL.data_connection_id = RRH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]     AS ILE ON ILE.[Document Type]    = 3 --Return Receipt
                                                            AND RRL.[Document No_]     = ILE.[Document No_]
                                                            AND RRL.[Line No_]         = ILE.[Document Line No_]
                                                            AND ILE.[Entry Type]       = 1 --Sales  
                                                            AND RRL.company_id         = ILE.company_id
                                                            AND RRL.data_connection_id = ILE.data_connection_id
   WHERE            RRL.execution_flag <> 'D'
     AND            RRH.execution_flag            <> 'D'
     AND            ILE.execution_flag            <> 'D' ;
GO
