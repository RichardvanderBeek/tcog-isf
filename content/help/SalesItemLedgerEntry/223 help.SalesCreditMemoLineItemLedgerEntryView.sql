/***********************************************************************************
Functionality:  This SQL script creates the help.SalesCreditMemoLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesCreditMemoLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.SalesCreditMemoLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    SCML.stage_id                                      AS StageID
                  , SCML.company_id                                    AS CompanyID
                  , SCML.data_connection_id                            AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                    AS EntryNo
                  , ILE.[Document Type]                                AS DocumentType
                  , ILE.[Document No_]                                 AS DocumentNo
                  , ILE.[Posting Date]                                 AS PostingDate
                  , COALESCE(SCMH.[Posting Date], SCML.[Posting Date]) AS DocumentPostingDate
                  , SCML.[Sell-to Customer No_]                        AS SelltoCustomerNo
                  , SCML.[Bill-to Customer No_]                        AS BilltoCustomerNo
                  , COALESCE(SCMH.[Currency Factor], 1)                AS CurrencyFactor
                  , SCMH.[Currency Code]                               AS CurrencyCode
                  /* ISF */
                  , ILE.[IsfBrand Code]                                AS BrandCode
                  , ILE.[IsfCollection Code]                           AS CollectionCode
                  , ILE.[IsfColor Code]                                AS ColorCode
                  , ILE.[IsfSeason Code]                               AS SeasonCode
                  , ILE.[IsfStyle No_]                                 AS StyleCode
                  , ILE.[IsfOrder Type]                                AS OrderTypeCode
                  , ILE.[IsfCup Code]                                  AS CupCode
                  , ILE.[IsfWidth Code]                                AS WidthCode
                  , ILE.[IsfLength]                                    AS LengthCode
                  , ILE.[IsfOrder Origin]                              AS OrderOriginCode
                  , ILE.[IsfQuality Code]                              AS QualityCode
    /* ISF */
    FROM            stage_nav.[Sales Cr_Memo Line]   AS SCML
    LEFT OUTER JOIN stage_nav.[Sales Cr_Memo Header] AS SCMH ON SCML.[Document No_]     = SCMH.[No_]
                                                            AND SCML.company_id         = SCMH.company_id
                                                            AND SCML.data_connection_id = SCMH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]    AS ILE ON ILE.[Document Type]      = 4 --Credit Memo
                                                           AND SCML.[Document No_]      = ILE.[Document No_]
                                                           AND SCML.[Line No_]          = ILE.[Document Line No_]
                                                           AND ILE.[Entry Type]         = 1 --Sales  
                                                           AND SCML.company_id          = ILE.company_id
                                                           AND SCML.data_connection_id  = ILE.data_connection_id
   WHERE            SCML.execution_flag <> 'D'
     AND            SCMH.execution_flag            <> 'D'
     AND            ILE.execution_flag             <> 'D' ;
GO
