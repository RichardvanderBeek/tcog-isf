/***********************************************************************************
Functionality:  This SQL script creates the help.LoadSalesItemLedgerEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadSalesItemLedgerEntry', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadSalesItemLedgerEntry
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  -- full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM help.SalesItemLedgerEntry ;
    INSERT INTO help.SalesItemLedgerEntry (
      --system information
      StageID
    , Company_ID
    , Data_Connection_ID
    , Component_Execution_ID
    , Execution_Flag
    , EntryNo
    , DocumentType
    , DocumentNo
    , PostingDate
    , DocumentPostingDate
    , SelltoCustomerNo
    , BilltoCustomerNo
    /* ISF */
    , BrandCode
    , CollectionCode
    , ColorCode
    , SeasonCode
    , StyleCode
    , OrderTypeCode
    , CupCode
    , WidthCode
    , LengthCode
    , OrderOriginCode
    , QualityCode
    /* ISF */
    )
    SELECT
      --system information
           SILUV.StageID
         , SILUV.CompanyID
         , SILUV.DataConnectionID
         , @component_execution_id
         , @execution_flag
         , SILUV.EntryNo
         , SILUV.DocumentType
         , SILUV.DocumentNo
         , SILUV.PostingDate
         , SILUV.DocumentPostingDate
         , SILUV.SelltoCustomerNo
         , SILUV.BilltoCustomerNo
         /* ISF */
         , SILUV.BrandCode
         , SILUV.CollectionCode
         , SILUV.ColorCode
         , SILUV.SeasonCode
         , SILUV.StyleCode
         , SILUV.OrderTypeCode
         , SILUV.CupCode
         , SILUV.WidthCode
         , SILUV.LengthCode
         , SILUV.OrderOriginCode
         , SILUV.QualityCode
      /* ISF */
      FROM help.SalesItemLedgerEntryUnionView AS SILUV ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;