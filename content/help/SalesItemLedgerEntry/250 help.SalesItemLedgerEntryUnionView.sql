/***********************************************************************************
Functionality:  This SQL script creates the help.SalesItemLedgerEntryUnionView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesItemLedgerEntryUnionView', @type = N'V' ;
GO
CREATE VIEW help.SalesItemLedgerEntryUnionView
AS
  SELECT SSLV.StageID
       , SSLV.CompanyID
       , SSLV.DataConnectionID
       , SSLV.EntryNo
       , SSLV.DocumentType
       , SSLV.DocumentNo
       , SSLV.PostingDate
       , SSLV.DocumentPostingDate
       , SSLV.SelltoCustomerNo
       , SSLV.BilltoCustomerNo
       /* ISF */
       , SSLV.BrandCode
       , SSLV.CollectionCode
       , SSLV.ColorCode
       , SSLV.SeasonCode
       , SSLV.StyleCode
       , SSLV.OrderTypeCode
       , SSLV.CupCode
       , SSLV.WidthCode
       , SSLV.LengthCode
       , SSLV.OrderOriginCode
       , SSLV.QualityCode
    /* ISF */
    FROM help.SalesShipmentLineItemLedgerEntryView AS SSLV
  UNION ALL
  SELECT SILV.StageID
       , SILV.CompanyID
       , SILV.DataConnectionID
       , SILV.EntryNo
       , SILV.DocumentType
       , SILV.DocumentNo
       , SILV.PostingDate
       , SILV.DocumentPostingDate
       , SILV.SelltoCustomerNo
       , SILV.BilltoCustomerNo
       /* ISF */
       , SILV.BrandCode
       , SILV.CollectionCode
       , SILV.ColorCode
       , SILV.SeasonCode
       , SILV.StyleCode
       , SILV.OrderTypeCode
       , SILV.CupCode
       , SILV.WidthCode
       , SILV.LengthCode
       , SILV.OrderOriginCode
       , SILV.QualityCode
    /* ISF */
    FROM help.SalesInvoiceLineItemLedgerEntryView AS SILV
  UNION ALL
  SELECT RRLV.StageID
       , RRLV.CompanyID
       , RRLV.DataConnectionID
       , RRLV.EntryNo
       , RRLV.DocumentType
       , RRLV.DocumentNo
       , RRLV.PostingDate
       , RRLV.DocumentPostingDate
       , RRLV.SelltoCustomerNo
       , RRLV.BilltoCustomerNo
       /* ISF */
       , RRLV.BrandCode
       , RRLV.CollectionCode
       , RRLV.ColorCode
       , RRLV.SeasonCode
       , RRLV.StyleCode
       , RRLV.OrderTypeCode
       , RRLV.CupCode
       , RRLV.WidthCode
       , RRLV.LengthCode
       , RRLV.OrderOriginCode
       , RRLV.QualityCode
    /* ISF */
    FROM help.ReturnReceiptLineItemLedgerEntryView AS RRLV
  UNION ALL
  SELECT SCMLV.StageID
       , SCMLV.CompanyID
       , SCMLV.DataConnectionID
       , SCMLV.EntryNo
       , SCMLV.DocumentType
       , SCMLV.DocumentNo
       , SCMLV.PostingDate
       , SCMLV.DocumentPostingDate
       , SCMLV.SelltoCustomerNo
       , SCMLV.BilltoCustomerNo
       /* ISF */
       , SCMLV.BrandCode
       , SCMLV.CollectionCode
       , SCMLV.ColorCode
       , SCMLV.SeasonCode
       , SCMLV.StyleCode
       , SCMLV.OrderTypeCode
       , SCMLV.CupCode
       , SCMLV.WidthCode
       , SCMLV.LengthCode
       , SCMLV.OrderOriginCode
       , SCMLV.QualityCode
    /* ISF */
    FROM help.SalesCreditMemoLineItemLedgerEntryView AS SCMLV
  UNION ALL
  SELECT SVEV.StageID
       , SVEV.CompanyID
       , SVEV.DataConnectionID
       , SVEV.EntryNo
       , SVEV.DocumentType
       , SVEV.DocumentNo
       , SVEV.PostingDate
       , SVEV.DocumentPostingDate
       , SVEV.SelltoCustomerNo
       , SVEV.BilltoCustomerNo
       /* ISF */
       , SVEV.BrandCode
       , SVEV.CollectionCode
       , SVEV.ColorCode
       , SVEV.SeasonCode
       , SVEV.StyleCode
       , SVEV.OrderTypeCode
       , SVEV.CupCode
       , SVEV.WidthCode
       , SVEV.LengthCode
       , SVEV.OrderOriginCode
       , SVEV.QualityCode
    /* ISF */
    FROM help.SalesItemLedgerEntryView AS SVEV ;
