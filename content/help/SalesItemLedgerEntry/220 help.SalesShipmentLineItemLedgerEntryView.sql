/***********************************************************************************
Functionality:  This SQL script creates the help.SalesShipmentLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesShipmentLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.SalesShipmentLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    SSSL.stage_id                                     AS StageID
                  , SSSL.company_id                                   AS CompanyID
                  , SSSL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                   AS EntryNo
                  , ILE.[Document Type]                               AS DocumentType
                  , ILE.[Document No_]                                AS DocumentNo
                  , ILE.[Posting Date]                                AS PostingDate
                  , COALESCE(SSH.[Posting Date], SSSL.[Posting Date]) AS DocumentPostingDate
                  , SSSL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , SSSL.[Bill-to Customer No_]                       AS BilltoCustomerNo
                  /* ISF */
                  , ILE.[IsfBrand Code]                               AS BrandCode
                  , ILE.[IsfCollection Code]                          AS CollectionCode
                  , ILE.[IsfColor Code]                               AS ColorCode
                  , ILE.[IsfSeason Code]                              AS SeasonCode
                  , ILE.[IsfStyle No_]                                AS StyleCode
                  , ILE.[IsfOrder Type]                               AS OrderTypeCode
                  , ILE.[IsfCup Code]                                 AS CupCode
                  , ILE.[IsfWidth Code]                               AS WidthCode
                  , ILE.[IsfLength]                                   AS LengthCode
                  , ILE.[IsfOrder Origin]                             AS OrderOriginCode
                  , ILE.[IsfQuality Code]                             AS QualityCode
    /* ISF */
    FROM            stage_nav.[Sales Shipment Line]   AS SSSL
    LEFT OUTER JOIN stage_nav.[Sales Shipment Header] AS SSH ON SSSL.[Document No_]     = SSH.[No_]
                                                            AND SSSL.company_id         = SSH.company_id
                                                            AND SSSL.data_connection_id = SSH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]     AS ILE ON ILE.[Document Type]     = 1 --Shipments
                                                            AND SSSL.[Document No_]     = ILE.[Document No_]
                                                            AND SSSL.[Line No_]         = ILE.[Document Line No_]
                                                            AND ILE.[Entry Type]        = 1 --Sales  
                                                            AND SSSL.company_id         = ILE.company_id
                                                            AND SSSL.data_connection_id = ILE.data_connection_id
   WHERE            SSSL.execution_flag <> 'D'
     AND            SSH.execution_flag             <> 'D'
     AND            ILE.execution_flag             <> 'D' ;
GO
