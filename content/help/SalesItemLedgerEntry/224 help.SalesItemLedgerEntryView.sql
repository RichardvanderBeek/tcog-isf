/***********************************************************************************
Functionality:  This SQL script creates the help.SalesItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.SalesItemLedgerEntryView
AS
  SELECT
    -- system information
         ILE.stage_id             AS StageID
       , ILE.Company_Id           AS CompanyID
       , ILE.data_connection_id   AS DataConnectionID
                                                  -- business key
       , ILE.[Entry No_]          AS EntryNo
       , 0                        AS DocumentType --Empty Document Type
       , ILE.[Document No_]       AS DocumentNo
       , ILE.[Posting Date]       AS PostingDate
       , ILE.[Posting Date]       AS DocumentPostingDate
       , ''                       AS BilltoCustomerNo
       , ''                       AS SelltoCustomerNo
                                                  /* ISF */
       , ILE.[IsfBrand Code]      AS BrandCode
       , ILE.[IsfCollection Code] AS CollectionCode
       , ILE.[IsfColor Code]      AS ColorCode
       , ILE.[IsfSeason Code]     AS SeasonCode
       , ILE.[IsfStyle No_]       AS StyleCode
       , ILE.[IsfOrder Type]      AS OrderTypeCode
       , ILE.[IsfCup Code]        AS CupCode
       , ILE.[IsfWidth Code]      AS WidthCode
       , ILE.[IsfLength]          AS LengthCode
       , ILE.[IsfOrder Origin]    AS OrderOriginCode
       , ILE.[IsfQuality Code]    AS QualityCode
    /* ISF */
    FROM stage_nav.[Item Ledger Entry] AS ILE
   WHERE ILE.[Entry Type]    = 1
     AND ILE.[Document Type] = 0
     AND ILE.execution_flag  <> 'D' ;
GO
