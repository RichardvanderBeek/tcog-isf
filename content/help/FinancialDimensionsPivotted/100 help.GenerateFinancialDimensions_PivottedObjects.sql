/****************************************************************************************************
  Functionality: This procedure will generate and deploy the database objects during a birds deploy 
    - 200 help.FinancialDimensionsPivottedView
    - 500 help.FinancialDimensionsPivotted
    - 700 help.LoadFinancialDimensionsPivotted

    The generated scripts can be viewed after running the generate procedure in @debug = 1 mode.
  Created by:    JVL	Date:	2019/01/17
  Date 		Changed by 	Ticket/Change 	Description
  2019/05/17 TvB changed length of data types to nvarchar(max) to ensure the field length is enough once more then 10 fin dims are used (DEV-2194)
*****************************************************************************************************/
EXEC dbo.drop_object @object = N'help.GenerateFinancialDimensions_PivottedObjects', @type = N'P' ;
GO
CREATE PROCEDURE help.GenerateFinancialDimensions_PivottedObjects @debug BIT = 0
AS
BEGIN
  SET NOCOUNT ON ;

  DECLARE @fin_dims                      NVARCHAR(MAX) = N''
        , @fin_dim_count                 INT           = 0
        , @fin_dims_incl_alias           NVARCHAR(MAX) = N''
        , @fin_dim_columns               NVARCHAR(MAX) = N''
        , @fin_dim_update_columns        NVARCHAR(MAX) = N''
        , @fin_dim_insert_select_columns NVARCHAR(MAX) = N''
        , @fin_dim_insert_columns        NVARCHAR(MAX) = N''
        , @fin_dim_id_columns            NVARCHAR(MAX) = N'' ;

  DECLARE @view_object_identifier      NVARCHAR(255) = N'help.FinancialDimensionsPivottedView'
        , @table_object_identifier     NVARCHAR(255) = N'help.FinancialDimensionsPivotted'
        , @procedure_object_identifier NVARCHAR(255) = N'help.LoadFinancialDimensionsPivotted' ;

  DECLARE @create_view      NVARCHAR(MAX)
        , @create_table     NVARCHAR(MAX)
        , @create_procedure NVARCHAR(MAX) ;




  SELECT @fin_dim_count = COUNT(DISTINCT financial_dimension_sequence)
    FROM meta.nav_financial_dimensions ;

  IF EXISTS (SELECT * FROM meta.nav_financial_dimensions)
  BEGIN ;
    WITH cte AS (
      SELECT DISTINCT financial_dimension_sequence FROM meta.nav_financial_dimensions
    )
    SELECT @fin_dims +=+QUOTENAME(cte.financial_dimension_sequence) + N','
         , @fin_dims_incl_alias += CHAR(10) + N', ' + QUOTENAME(cte.financial_dimension_sequence) + N' AS DimensionValueNo'
                                   + CAST(cte.financial_dimension_sequence AS NVARCHAR(10))
         , @fin_dim_columns += CHAR(10) + N', DimensionValueNo' + CAST(cte.financial_dimension_sequence AS NVARCHAR(10)) + N' INT NULL'
         , @fin_dim_update_columns += CHAR(10) + N', DimensionValueNo' + CAST(cte.financial_dimension_sequence AS NVARCHAR(10)) + N' = FDPV.DimensionValueNo'
                                      + CAST(cte.financial_dimension_sequence AS NVARCHAR(10))
         , @fin_dim_insert_select_columns += CHAR(10) + N', FDPV.DimensionValueNo' + CAST(cte.financial_dimension_sequence AS NVARCHAR(10))
         , @fin_dim_insert_columns += CHAR(10) + N', DimensionValueNo' + CAST(cte.financial_dimension_sequence AS NVARCHAR(10))
         , @fin_dim_id_columns += CHAR(10) + N', FinancialDimension' + CAST(cte.financial_dimension_sequence AS NVARCHAR(10)) + N'ID INT NULL'
      FROM cte ;

    SELECT @fin_dims = LEFT(@fin_dims, LEN(@fin_dims) - 1) ;
  END ;
  /****************************************************************************************************
      Functionality:  Create the View SQL 
  *****************************************************************************************************/

  SELECT @create_view = N'CREATE VIEW ' + @view_object_identifier + CHAR(10) + N'AS' ;

  SELECT @create_view += CHAR(10)
                         + N' WITH financial_dimensions AS (
  SELECT dse.data_connection_id                                      AS DataConnectionId
       , dse.company_id                                              AS CompanyId
       -- bk
       , fd.financial_dimension_sequence                             AS DimensionGlobalSequence
       , dse.[Dimension Set ID]                                      AS DimensionSetNo
       , dse.[Dimension Value ID]                                    AS DimensionValueNo
    FROM stage_nav.[Dimension Set Entry] AS dse
    JOIN stage_nav.[Dimension Value]     AS dv ON dse.[Dimension Value ID] = dv.[Dimension Value ID]
                                              AND dse.[Dimension Code]     = dv.[Dimension Code]
                                              AND dse.company_id           = dv.company_id
                                              AND dse.data_connection_id   = dv.data_connection_id
    JOIN meta.nav_financial_dimensions   AS fd ON dse.[Dimension Code]     = fd.dimension_code
                                              AND dse.company_id           = fd.company_id
     WHERE dse.execution_flag <> ''D''
     AND dv.execution_flag  <> ''D'' )' ;

  SELECT @create_view +=+CHAR(10) + N'SELECT DataConnectionId, CompanyId, DimensionSetNo ' + @fin_dims_incl_alias + CHAR(10)
                        + N'FROM ( SELECT DataConnectionId, CompanyId, DimensionSetNo, DimensionGlobalSequence, DimensionValueNo FROM financial_dimensions ) AS SourceTable  '
                        + CASE
                            WHEN @fin_dim_count > 0 THEN
                              CHAR(10) + 'PIVOT( MIN(DimensionValueNo) FOR DimensionGlobalSequence IN (' + @fin_dims + ') ) AS PivotTable;'
                            ELSE ''
                          END ;


  EXEC dbo.drop_object @object = @view_object_identifier, @type = N'V', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_view ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN

EXEC sys.sp_executesql @create_view ;
  END ;


  /****************************************************************************************************
      Functionality:  Create the Table SQL and Deploy the Table  
  *****************************************************************************************************/

  SELECT @create_table = N'CREATE TABLE ' + @table_object_identifier + CHAR(10) + N'(' ;

  SELECT @create_table +=+CHAR(10) + N' DataConnectionID INT NOT NULL ' + CHAR(10) + N', CompanyID INT NOT NULL ' + CHAR(10)
                         + N', ComponentExecutionID INT NOT NULL ' + CHAR(10) + N', DimensionSetNo INT NOT NULL ' + CHAR(10) + @fin_dim_columns + CHAR(10)
                         + @fin_dim_id_columns ;
  SELECT @create_table += N')' ;


  EXEC dbo.drop_object @object = @table_object_identifier, @type = N'T', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_table ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN

EXEC sys.sp_executesql @create_table ;
  END ;

  /****************************************************************************************************
      Functionality:  Create the Procedure SQL and Deploy the Procedure  
  *****************************************************************************************************/

  SELECT @create_procedure
    = N'CREATE PROCEDURE ' + @procedure_object_identifier + CHAR(10)
      + N'@component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS'                          + CHAR(10) + N'
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ; ' ;

  SELECT @create_procedure += N'  UPDATE      ' + @table_object_identifier + N' SET' + CHAR(10) + N' ComponentExecutionId = @component_execution_id '
                              + @fin_dim_update_columns ;

  SELECT @create_procedure += N'  FROM      ' + @view_object_identifier + N' AS FDPV
   INNER JOIN '               + @table_object_identifier
                              + N'     AS FDP ON FDPV.DimensionSetNo   = FDP.DimensionSetNo
                                                AND FDPV.CompanyId        = FDP.CompanyId
                                                AND FDPV.DataConnectionId = FDP.DataConnectionId ;
 SELECT @updated = @@ROWCOUNT ;' ;

  SELECT @create_procedure += CHAR(10) + CHAR(10) + N'
  INSERT INTO '               + @table_object_identifier + N' (
    ComponentExecutionId
  , CompanyId
  , DataConnectionId
  , DimensionSetNo '          + @fin_dim_insert_columns + CHAR(10) + N')' + CHAR(10) + N'SELECT ' + CHAR(10) + N'@component_execution_id' + CHAR(10)
                              + N', FDPV.CompanyId ' + CHAR(10) + N', FDPV.DataConnectionId ' + CHAR(10) + N', FDPV.DimensionSetNo '
                              + @fin_dim_insert_select_columns ;

  SELECT @create_procedure += N'  FROM ' + @view_object_identifier + N' AS FDPV
   WHERE NOT EXISTS ( SELECT *
                        FROM ' + @table_object_identifier
                              + N' AS FDP
                       WHERE FDPV.DimensionSetNo   = FDP.DimensionSetNo
                         AND FDPV.CompanyId        = FDP.CompanyId
                         AND FDPV.DataConnectionId = FDP.DataConnectionId) ;
  SELECT @inserted = @@ROWCOUNT ;' ;

  SELECT @create_procedure +=+CHAR(10) + N'END ;' ;


  EXEC dbo.drop_object @object = @procedure_object_identifier, @type = N'P', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_procedure ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN

EXEC sys.sp_executesql @create_procedure ;
  END ;

END ;


GO

EXEC help.GenerateFinancialDimensions_PivottedObjects @debug = 0 ;