/***********************************************************************************
Functionality:  This SQL script creates the help.ReturnReceiptLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.ReturnReceiptLineView', @type = N'V' ;
GO
CREATE VIEW help.ReturnReceiptLineView
AS
  SELECT
    -- system information
                    RRL.stage_id                                     AS StageID
                  , RRL.company_id                                   AS CompanyID
                  , RRL.data_connection_id                           AS DataConnectionID
                  , VE.execution_timestamp                           AS execution_timestamp
                  -- business key
                  , VE.[Entry No_]                                   AS EntryNo
                  , VE.[Document Type]                               AS DocumentType
                  , VE.[Document No_]                                AS DocumentNo
                  , VE.[Posting Date]                                AS PostingDate
                  , COALESCE(RRH.[Posting Date], RRL.[Posting Date]) AS DocumentPostingDate
                  , RRL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , RRL.[Bill-to Customer No_]                       AS BilltoCustomerNo
                  , COALESCE(RRH.[Currency Factor], 1)               AS CurrencyFactor
                  , RRH.[Currency Code]                              AS CurrencyCode
                  , RRL.[Type]                                       AS [Type]
                  /* ISF */
                  , VE.[IsfBrand Code]                               AS BrandCode
                  , VE.[IsfCollection Code]                          AS CollectionCode
                  , VE.[IsfColor Code]                               AS ColorCode
                  , VE.[IsfSeason Code]                              AS SeasonCode
                  , VE.[IsfStyle No_]                                AS StyleCode
                  , VE.[IsfOrder Type]                               AS OrderTypeCode
                  , VE.[IsfCup Code]                                 AS CupCode
                  , VE.[IsfWidth Code]                               AS WidthCode
                  , VE.[IsfLength]                                   AS LengthCode
                  , VE.[IsfOrder Origin]                             AS OrderOriginCode
                  , VE.[IsfQuality Code]                             AS QualityCode
                  , ''                                               AS ShippingWindowCode
    /* ISF */
    FROM            stage_nav.[Return Receipt Line]   AS RRL
    LEFT OUTER JOIN stage_nav.[Return Receipt Header] AS RRH ON RRL.[Document No_]         = RRH.[No_]
                                                            AND RRL.company_id             = RRH.company_id
                                                            AND RRL.data_connection_id     = RRH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]           AS VE ON VE.[Document Type]          = 3 --Return receipts
                                                           AND RRL.[Document No_]          = VE.[Document No_]
                                                           AND RRL.[Line No_]              = VE.[Document Line No_]
                                                           AND VE.[Item Ledger Entry Type] = 1 --Sales
                                                           AND RRL.company_id              = VE.company_id
                                                           AND RRL.data_connection_id      = VE.data_connection_id
   WHERE            RRL.execution_flag <> 'D'
     AND            RRH.execution_flag            <> 'D'
     AND            VE.execution_flag             <> 'D' ;
GO
