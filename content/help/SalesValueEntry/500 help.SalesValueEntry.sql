/***********************************************************************************
Functionality:  This SQL script creates the help.SalesValueEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesValueEntry', @type = N'T' ;
GO
CREATE TABLE help.SalesValueEntry
(
  -- system information
  StageID                BIGINT         NOT NULL
, CompanyID              INT            NOT NULL
, DataConnectionID       INT            NOT NULL
, ComponentExecutionID   INT            NOT NULL
, execution_timestamp    BINARY(8)      NOT NULL
, Execution_Flag         NVARCHAR(10)   NOT NULL
, EntryNo                INT            NOT NULL
, DocumentType           INT            NOT NULL
, DocumentNo             NVARCHAR(100)  NULL
, PostingDate            DATETIME
, DocumentPostingDate    DATETIME
, SelltoCustomerNo       NVARCHAR(100)  NULL
, BilltoCustomerNo       NVARCHAR(100)  NULL
, CurrencyFactor         DECIMAL(19, 4) NULL
, CurrencyCode           NVARCHAR(100)  NULL
, [Type]                 INT            NULL
/* ISF */
, BrandCode              NVARCHAR(100)  NULL
, CollectionCode         NVARCHAR(100)  NULL
, ColorCode              NVARCHAR(100)  NULL
, SeasonCode             NVARCHAR(100)  NULL
, StyleCode              NVARCHAR(100)  NULL
, OrderTypeCode          NVARCHAR(100)  NULL
, CupCode                NVARCHAR(100)  NULL
, WidthCode              NVARCHAR(100)  NULL
, LengthCode             NVARCHAR(100)  NULL
, OrderOriginCode        NVARCHAR(100)  NULL
, QualityCode            NVARCHAR(100)  NULL
, ShippingWindowCode     NVARCHAR(100)  NULL
/* ISF */
) ;