EXEC dbo.drop_object @object = N'help.AddAllGLAccountHierarchy', @type = N'P' ;
GO
CREATE PROCEDURE help.AddAllGLAccountHierarchy
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  DELETE FROM help.GLAccountHierarchy;
  SET @deleted = @@ROWCOUNT ;

  --Example usage P&L and Balance Accounts
  --EXEC help.AddGLAccountHierarchy @AccountType = 'P&L', @Level1Account = '6500', @Level1Desc = 'Gross Margin', @Level2Account = '6600', @Level2Desc = 'Contribution Margin', @Level3Account = '6700', @Level3Desc = 'EBITDA', @Level4Account = '6800', @Level4Desc = 'EBIT', @Level5Account = '6900', @Level5Desc = 'PBT', @Level6Account = '7000', @Level6Desc = 'Net Income', @Level7Account = '400000', @Level7Desc = 'Sales', @Level8Account = '411000', @Level8Desc = 'Product Sales', @GLAccount = '411100', @GLAccountDesc = 'Product sales', @component_execution_id = @component_execution_id;
  --EXEC help.AddGLAccountHierarchy @AccountType = 'Balance', @Level1Account = '10000', @Level1Desc = 'ASSETS', @Level2Account = '100000', @Level2Desc = 'Current Assets', @Level3Account = '110000', @Level3Desc = 'Cash & Equivalents', @Level4Account = '', @Level4Desc = '', @Level5Account = '', @Level5Desc = '', @Level6Account = '', @Level6Desc = '', @Level7Account = '', @Level7Desc = '', @Level8Account = '', @Level8Desc = '', @GLAccount = '111000', @GLAccountDesc = 'Petty cash', @component_execution_id = @component_execution_id;

  SELECT @inserted = COUNT(*) FROM help.GLAccountHierarchy

END;
