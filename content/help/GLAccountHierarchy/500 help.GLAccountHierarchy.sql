EXEC dbo.drop_object @object = N'help.GLAccountHierarchy', @type = N'T' ;
GO
CREATE TABLE help.GLAccountHierarchy
(
  GLAccountHierarchyId BIGINT        IDENTITY(1, 1) NOT NULL
, ComponentExecutionId INT           NOT NULL
, DataConnectionId     INT           NOT NULL
, AccountType          NVARCHAR(255) NOT NULL
, Level1Account        NVARCHAR(255) NOT NULL
, Level1Desc           NVARCHAR(255) NOT NULL
, Level2Account        NVARCHAR(255) NOT NULL
, Level2Desc           NVARCHAR(255) NOT NULL
, Level3Account        NVARCHAR(255) NOT NULL
, Level3Desc           NVARCHAR(255) NOT NULL
, Level4Account        NVARCHAR(255) NOT NULL
, Level4Desc           NVARCHAR(255) NOT NULL
, Level5Account        NVARCHAR(255) NOT NULL
, Level5Desc           NVARCHAR(255) NOT NULL
, Level6Account        NVARCHAR(255) NOT NULL
, Level6Desc           NVARCHAR(255) NOT NULL
, Level7Account        NVARCHAR(255) NOT NULL
, Level7Desc           NVARCHAR(255) NOT NULL
, Level8Account        NVARCHAR(255) NOT NULL
, Level8Desc           NVARCHAR(255) NOT NULL
, GLAccount            NVARCHAR(255) NOT NULL
, GLAccountDesc        NVARCHAR(255) NOT NULL
) ;