EXEC dbo.drop_object @object = N'help.FinancialDimensionsView', @type = N'V' ;
GO
CREATE VIEW help.FinancialDimensionsView
AS
  SELECT fd.financial_dimension_id                                   AS FinancialDimensionsId
       , dse.data_connection_id                                      AS DataConnectionId
       , dse.company_id                                              AS CompanyId
       -- bk
       , dse.[Dimension Code]                                        AS DimensionCode
       , fd.financial_dimension_sequence                             AS DimensionGlobalSequence
       , dse.[Dimension Set ID]                                      AS DimensionSetNo
       , dse.[Dimension Value ID]                                    AS DimensionValueNo
       -- attributes
       , COALESCE(dv.Code, 'N/A')                                    AS DimensionValueCode
       , COALESCE(dv.Name, 'N/A')                                    AS DimensionValueName
       , COALESCE(dv.Code, 'N/A') + ' - ' + COALESCE(dv.Name, 'N/A') AS DimensionValueCodeName
    FROM stage_nav.[Dimension Set Entry] AS dse
    JOIN stage_nav.[Dimension Value]     AS dv ON dse.[Dimension Value ID] = dv.[Dimension Value ID]
                                              AND dse.[Dimension Code]     = dv.[Dimension Code]
                                              AND dse.company_id           = dv.company_id
                                              AND dse.data_connection_id   = dv.data_connection_id
    JOIN meta.nav_financial_dimensions   AS fd ON dse.[Dimension Code]     = fd.dimension_code
                                              AND dse.company_id           = fd.company_id
   WHERE dse.execution_flag <> 'D'
     AND dv.execution_flag  <> 'D' ;
GO
