EXEC dbo.drop_object @object = N'help.GenerateTableFinancialDimensionHierarchies', @type = N'P' ;
GO
CREATE PROCEDURE help.GenerateTableFinancialDimensionHierarchies
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @debug                  BIT      = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN

  DECLARE @columns NVARCHAR(MAX) = N'' ;
  DECLARE @table_object_identifier NVARCHAR(255) = N'help.FinancialDimensionHierarchies' ;
  DECLARE @create_table NVARCHAR(MAX) ;


  WITH hierarchy_levels (lvl) AS (
    SELECT help.GetFinancialDimensionHierachiesMaxLevel() -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @columns += CHAR(10) + N', LevelCode' + CAST(cte.lvl AS NVARCHAR(10)) + N' NVARCHAR(50) NULL' + CHAR(10) + N', LevelName'
                     + CAST(cte.lvl AS NVARCHAR(10)) + N' NVARCHAR(100) NULL'
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl ASC ;

  /****************************************************************************************************
      Functionality:  Create the Table SQL and Deploy the Table  
  *****************************************************************************************************/

  SELECT @create_table = N'CREATE TABLE ' + @table_object_identifier + CHAR(10) + N'(' ;

  SELECT @create_table +=+CHAR(10) + N' StageID INT NOT NULL ' + CHAR(10) + N', DataConnectionID INT NOT NULL ' + CHAR(10) + N', CompanyID INT NOT NULL '
                         + CHAR(10) + N', ComponentExecutionID INT NOT NULL ' + CHAR(10) + N', DimensionCode NVARCHAR(50) NOT NULL ' + CHAR(10)
                         + N', Code NVARCHAR(50) NOT NULL ' + CHAR(10) + N', Name NVARCHAR(100) NOT NULL ' + CHAR(10) + @columns ;
  SELECT @create_table += N')' ;


  EXEC dbo.drop_object @object = @table_object_identifier, @type = N'T', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_table ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN
EXEC sys.sp_executesql @create_table ;
  END ;
  EXEC dwh.initialize_component @component_name = N'helpFinancialDimensionHierarchies' -- nvarchar(50)
                              , @component_category = N'help'                          -- nvarchar(20)
                              , @debug = @debug ;                                      -- bit     
END ;

GO

EXEC help.GenerateTableFinancialDimensionHierarchies @debug = 0 ;
